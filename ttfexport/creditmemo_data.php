<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Credit Memo</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
}
body {
	background-color: #FFF;
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
</style>
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
table.gridtable td.right {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
</head>

<body>
<?php
// Include Magento application
require_once ( "../app/Mage.php" );
//umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$readConnection = Mage::getSingleton("core/resource")->getConnection("core_read");
$RmaNo = $_GET["id"];
$DataOrderNo = "";
$query = "Select *, (grand_total - tax_amount) as grand_total_excltax From sales_flat_creditmemo Where entity_id='" . (int)$RmaNo . "'  LIMIT 1";
if ($OrderArrays = $readConnection->fetchAll($query)) {
  foreach ($OrderArrays as $orderRow) {
   echo "<h2>Credit Memo # : ". $orderRow["increment_id"] ."</h2>";
   echo "<p><strong>Status : </strong>" . $orderRow["state"] . "&nbsp;,&nbsp;<strong>Order ID</strong># : " . $orderRow["order_id"] . "</p>";
   echo "<a href=\"/ttfexport/creditmemos.php\">Back to Credit Memo List</a>&nbsp;|&nbsp;";
?>
<table cellpadding="1" cellspacing="1" width="100%" border="0">
   <tr>
   	   <td><strong>Order No # 
			<?php
			$query = "Select * From sales_flat_order Where (entity_id=". (int)$orderRow["order_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
				   echo $addrRow["increment_id"] . "<br/>";
				   $DataOrderNo = $addrRow["increment_id"];
				}
			}
			else {
				echo "&nbsp;";
			}
			?>	   
	   
	   </strong></td>	
	   <td>&nbsp;&nbsp;</td>
   		<td><strong>Account Information</strong></td>
   </tr>
   <tr>
   	   <td style="padding-left:10px;">
			<?php
			$query = "Select * From sales_flat_order Where (entity_id=". (int)$orderRow["order_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
				   echo "Order Date : " . $addrRow["created_at"] . "<br/>";
				   echo "Order Status : " . $addrRow["state"] . " -> " .  $addrRow["status"] . "<br/>";
				   echo "Purchased From : " . preg_replace("/\s\s+/", " &raquo; ", $addrRow["store_name"]) . "<br/>";
				   echo "Placed from IP : " . $addrRow["remote_ip"] . "<br/>";
				}
			}
			else {
				echo "&nbsp;";
			}
			?>			   
	   </td>	
	   <td>&nbsp;</td>
   		<td>
			<?php
			$query = "SELECT sales_flat_order.*, customer_entity.email, customer_entity.group_id, customer_group.customer_group_code " .
					" FROM sales_flat_order " .
					" Left Join customer_entity ON sales_flat_order.customer_id = customer_entity.entity_id " .
					" Left Join customer_group ON customer_entity.group_id = customer_group.customer_group_id " .
					"  Where (sales_flat_order.entity_id=". (int)$orderRow["order_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
				   echo "Customer Name : " . $addrRow["customer_firstname"] . " " . $addrRow["customer_lastname"] . "<br/>";
				   echo "Email : " . $addrRow["email"] . "<br/>";
				   echo "Customer Group : " . $addrRow["customer_group_code"] . "<br/>";
				}
			}
			else {
				echo "&nbsp;";
			}
			?>	
		</td>
   </tr>   
   <tr>
   	   <td><strong>Billing Address</strong></td>	
	   <td>&nbsp;&nbsp;</td>
   		<td><strong>Shipping Address</strong></td>
   </tr> 
   <tr>
   	   <td style="padding-left:10px;">
			<?php
			$query = "Select * From sales_flat_order_address Where (entity_id=". (int)$orderRow["billing_address_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
			   	   if (trim($addrRow["company"]) !="") {
				   		echo trim($addrRow["company"]) . "<br/>";
				   }
				   echo trim($addrRow["prefix"] . " " . $addrRow["firstname"] . " " . $addrRow["middlename"] . " " . $addrRow["lastname"] . " " . $addrRow["suffix"]) . "<br/>";
				   echo trim($addrRow["street"]);
				   echo trim($addrRow["city"] . ", " . $addrRow["region"] . ", " . $addrRow["postcode"]) . "<br/>";
				   echo trim($addrRow["country_id"]) . "<br/>";
			   }
			}
			else {
				echo "&nbsp;";
			}
			?>			   
	   </td>	
	   <td>&nbsp;&nbsp;</td>
   		<td style="padding-left:10px;">
			<?php
			$query = "Select * From sales_flat_order_address Where (entity_id=". (int)$orderRow["shipping_address_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
			   	   if (trim($addrRow["company"]) !="") {
				   		echo trim($addrRow["company"]) . "<br/>";
				   }
				   echo trim($addrRow["prefix"] . " " . $addrRow["firstname"] . " " . $addrRow["middlename"] . " " . $addrRow["lastname"] . " " . $addrRow["suffix"]) . "<br/>";
				   echo trim($addrRow["street"]);
				   echo trim($addrRow["city"] . ", " . $addrRow["region"] . ", " . $addrRow["postcode"]) . "<br/>";
				   echo trim($addrRow["country_id"]) . "<br/>";
			   }
			}
			else {
				echo "&nbsp;";
			}
			?>		
		</td>
   </tr>    
   
   <tr>
   	   <td><strong>Payment Information</strong></td>	
	   <td>&nbsp;&nbsp;</td>
   		<td><strong>Shipping Information</strong></td>
   </tr>
   <tr>
   	   <td style="padding-left:10px;">
			<?php
			$query = "Select * From  sales_flat_order_payment Where (parent_id=". (int)$orderRow["order_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
				   echo Mage::getStoreConfig("payment/". $addrRow["method"] . "/title") . "<br/>";
				   echo "Ship Carrier :  " . $addrRow["method"] . "<br/>";
				}
			}
			else {
				echo "&nbsp;";
			}
			?>			   
	   </td>	
	   <td>&nbsp;</td>
   		<td>
			<?php
			$query = "Select * From sales_flat_order Where (entity_id=". (int)$orderRow["order_id"] .")";
			if ($AccArrays = $readConnection->fetchAll($query)) {
			   foreach ($AccArrays as $addrRow) {	
				   echo $addrRow["shipping_description"] . "&nbsp;&nbsp; Total Shipping Charges: " . number_format($addrRow["shipping_amount"], 2, ".", ",") . "<br/>";
				   echo "Ship Carrier :  " . $addrRow["cadp_ship_carrier"] . "<br/>";
				}
			}
			else {
				echo "&nbsp;";
			}
			?>		
		</td>
   </tr>   
   
   <tr>
   		<td colspan="3" style="padding-right:10px;">
			<strong>Items Refunded</strong>
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
				<tr>
					<td><strong>No.</strong></td>
					<td><strong>SKU</strong></td>
					<td><strong>Product Name</strong></td>
					<td><strong>Price</strong></td>
					<td><strong>Qty</strong></td>
					<td><strong>Subtotal</strong></td>
					<td><strong>Tax Amount</strong></td>
					<td><strong>Discount Amount</strong></td>
					<td><strong>Row Total</strong></td>
				</tr>	
				<?php
				$ItemNo  = 1;
				$query = "Select *, (qty*price_incl_tax) as subtotal From sales_flat_creditmemo_item Where (parent_id=". (int)$RmaNo .") Order By entity_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {								
				?>
				<tr>			
					<td align="center"><?php echo $ItemNo;?></td>
					<td><?php echo $row["sku"];?></td>
					<td><?php echo $row["name"];?></td>
					<td align="right" align="center"><?php echo number_format($row["price_incl_tax"], 2, ".", ",");?></td>
					<td align="center"><?php echo number_format($row["qty"], 0, ".", ",");?></td>
					<td align="right" align="center"><?php echo number_format($row["subtotal"], 2, ".", ",");?></td>
					<td align="right"><?php echo number_format($row["tax_amount"], 2, ".", ",");?></td>
					<td align="right"><?php echo number_format($row["discount_amount"], 2, ".", ",");?></td>
					<td align="right"><?php echo  number_format($row["row_total_incl_tax"], 2, ".", ",");?></td>
				</tr>				
				<?php
						$ItemNo++;
					}
				}
				?>						
			</table>
		</td>
   </tr>   
   <tr>
   		<td valign="top">
		   <?php
		   $oldRefundType = $orderRow["ttf_refundtype"];
		   $oldAccBank = $orderRow["ttf_refundbank"];
		   $oldAccName = $orderRow["ttf_refundaccountname"];
		   $oldAccNumber = $orderRow["ttf_refundaccountno"];
		   if ($orderRow["ttf_crs"] == "Y") {
		   ?>
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
				<tr>
					<td>Refund Type : </td>
					<td><?php echo $oldRefundType;?></td>
				</tr>
				<tr>
					<td>Bank : </td>
					<td><?php echo $oldAccBank;?></td>
				</tr>
				<tr>
					<td>Account Name : </td>
					<td><?php echo $oldAccName;?></td>
				</tr>
				<tr>
					<td>Account Number : </td>
					<td><?php echo $oldAccNumber;?></td>
				</tr>
			</table>		   
		   <?
		   }
		   else {
		   ?>
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
			   <form name="memesavefrm" action="creditmemo_save.php" method="post">
				<tr>
					<td>Refund Type : </td>
					<td>
						<select name="order_refund_type" class="select" id="order_refund_type" style="width:100%;">
			        		<option value="none"><?php echo Mage::helper('sales')->__('-- select --') ?></option>  
			                <option value="cash" <?php echo ($oldRefundType=="cash") ? "  selected=\"selected\"" : "";?>>Cash</option>
			                <option value="eft" <?php echo ($oldRefundType=="eft") ? "  selected=\"selected\"" : "";?>>Bank Transfer</option>
			                <option value="2c2p" <?php echo ($oldRefundType=="2c2p") ? "  selected=\"selected\"" : "";?>>Credit Card</option>
			                <option value="storecredit" <?php echo ($oldRefundType=="storecredit") ? "  selected=\"selected\"" : "";?>>Store Credit</option>
			            </select>					
					</td>
				</tr>
				<tr>
					<td>Bank : </td>
					<td>
						<select name="order_ebt_acc_bank" class="select" id="order_ebt_acc_bank" style="width:100%;">
			        		<option value="none"><?php echo Mage::helper('sales')->__('-- select --') ?></option>
			        		<?php 
			        			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			        			$q_string = '';
			        			if(Mage::app()->getLocale()->getLocaleCode() == "th_TH") {
			        				$q_string = "SELECT initial, bank_name_th as bank_name FROM ttakeoff_bank_code ORDER BY bank_name_th";
			        			} else {
			        				$q_string = "SELECT initial, bank_name FROM ttakeoff_bank_code ORDER BY bank_name";
			        			}
			        			$q_entity = $read->fetchAll($q_string);
			        			foreach($q_entity as $entity):
			        		?>
			                	<option value="<?php echo trim($entity['initial']); ?>" <?php echo ($oldAccBank == trim($entity['initial'])) ? ' selected="selected"' : ''; ?>>
			                		<?php echo trim($entity['bank_name']); ?>
			                	</option>
			            	<?php endforeach; //print_r($q_entity); ?>
			            </select>					
					</td>
				</tr>
				<tr>
					<td>Account Name : </td>
					<td>
						<input type="text" name="order_ebt_acc_name" style="width:99%;" id="order_ebt_acc_name" value="<?php echo $oldAccName;?>"></input>
					</td>
				</tr>
				<tr>
					<td>Account Number : </td>
					<td>
						<input type="text" name="order_ebt_acc_number" style="width:99%;" id="order_ebt_acc_number" value="<?php echo $oldAccNumber;?>"></input>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center">
							<input type="hidden" name="id" value="<?php echo $RmaNo;?>">
							<input type="hidden" name="xid" value="<?php echo $orderRow["increment_id"];?>">
							<input type="submit" name="btnSubmit" value="Save">
							<input type="reset" name="btnReset" value="Reset">
					
					</td>
				</tr>
			  </form>
			</table>
			<?php
			}
			?>
			
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
				<tr>
					<td>
					     <?php if (trim($oldRefundType) !="") {?>
							<p><b><i>สร้าง CRS & GL กรณีคืนของบางส่วน</i></b></p>
							<ul>
								<?php if ($orderRow["ttf_crs"] != "Y") {?>
								<li><a href="../creditmemo_action.php?cmno=<?php echo $orderRow["increment_id"] . "&id=" . $RmaNo;?>&process=1">1. Create CRS</a><br/></li>
								<?php
								}
								else {
								?>
									<li>1. Create CRS - <img src="check.png" width="16" height="16" alt="" align="absmiddle"></li>
								<?php
								}
								?>
								<?php if ($orderRow["ttf_gl_return"] != "Y") {?>
								<li><a href="../creditmemo_action.php?cmno=<?php echo $orderRow["increment_id"] . "&id=" . $RmaNo;?>&process=2">2. Create GL - Customer Return</a></li>
								<?php
								}
								else {
								?>
									<li>2. Create GL - Customer Return -  <img src="check.png" width="16" height="16" alt="" align="absmiddle"></li>								
								<?php
								}
								?>
								<?php if ($orderRow["ttf_gl_refund"] != "Y") {?>
								<li><a href="../creditmemo_action.php?cmno=<?php echo $orderRow["increment_id"]. "&id=" . $RmaNo;?>&process=3">3. Create GL - Customer Refund</a></li>
								<?php
								}
								else {
								?>
									<li>3. Create GL - Customer Refund -  <img src="check.png" width="16" height="16" alt="" align="absmiddle"></li>			
								<?php
								}
								?>
							</ul>
						 <?php
						 }
						 else {
						 	echo "<p>กรุณากรอกข้อมูล Refund Type ก่อนสร้าง CRS & GL</p>";
						 }
						 ?>
					</td>
				</tr>			
			</table>
		</td>
		<td>&nbsp;&nbsp;</td>
		<td width="50%" valign="top" style="padding-right:10px;">
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
				<tr>
					<td align="right"><strong>Subtotal</strong></td>
					<td align="right"><?php echo number_format($orderRow["subtotal_incl_tax"], 2, ".", ",");?></td>					
				</tr>		
				<tr>
					<td align="right"><strong>Adjustment Refund</strong></td>
					<td align="right"><?php echo number_format($orderRow["adjustment_positive"], 2, ".", ",");?></td>					
				</tr>		
				<tr>
					<td align="right"><strong>Adjustment Fee</strong></td>
					<td align="right"><?php echo number_format($orderRow["adjustment_negative"], 2, ".", ",");?></td>					
				</tr>		
				<tr>
					<td align="right"><strong>Grand Total (Excl.Tax)</strong></td>
					<td align="right"><?php echo number_format($orderRow["grand_total_excltax"], 2, ".", ",");?></td>					
				</tr>	
				<tr>
					<td align="right"><strong>Total Tax</strong></td>
					<td align="right"><?php echo number_format($orderRow["tax_amount"], 2, ".", ",");?></td>					
				</tr>							
				<tr>
					<td align="right"><strong>Grand Total (Incl.Tax)</strong></td>
					<td align="right"><?php echo number_format($orderRow["grand_total"], 2, ".", ",");?></td>					
				</tr>	
			  </table>		
		</td>
   </tr>
</table>
<h2>CRS & GL Data</h2>
<table width="100%" cellpadding="1" cellspacing="1" border="0">
	<tr>
		<td><strong>CRS Data</strong></td>
		<td>&nbsp;&nbsp;</td>
		<td><strong>GL Data</strong></td>
	</tr>
	<tr>
		<td valign="top">
			<?php
				$OrderNo = $DataOrderNo;
				$query = "Select * From ttakeoff_csr Where order_id='" . mysql_escape_string($OrderNo) . "'  LIMIT 100";
				//echo "<p>". $query  ."</p>";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				  echo "<table cellpadding=\"1\" cellspacing=\"1\" border=\"0\">";
				  echo "<tr>";	
				   foreach ($AccArrays as $row) {
				     echo "<td>";	
				   	 echo "<ul>"; 		
				 		echo "<li><strong>Batch Id = <u>" . $row["batch_id"] . "</u></strong></li>";
				 		echo "<li>BATCH_NO = " . $row["batch_no"] . "</li>";
				 		echo "<li>STORE_NO = " . $row["strore_no"] . "</li>";
				 		echo "<li>ORDER_DATE = " . $row["order_date"] . "</li>";
				 		echo "<li>ORDER_TIME = " . $row["order_time"] . "</li>";
				 		echo "<li>ORDER_TYPE = " . $row["order_type"] . "</li>"; 
						
						echo "<li>Broker_ID = " . $row["broker_id"] . "</li>"; 
						echo "<li>Invoice Date = " . $row["invoice_date"] . "</li>"; 
						echo "<li>merchant_id = " . $row["merchant_id"] . "</li>"; 
						echo "<li>order_id = " . $row["order_id"] . "</li>"; 
						
						echo "<li>invoice_no = " . $row["invoice_no"] . "</li>"; 
						echo "<li>Credit Note No. = " . $row["credit_note_no"] . "</li>"; 
						echo "<li>Credit Note Date = " . $row["credit_note_date"] . "</li>"; 
						echo "<li>Payment Model = " . $row["payment_model"] . "</li>"; 
						echo "<li>payment_channel = " . $row["payment_channel"] . "</li>"; 
						
						echo "<li>ChannelCode = " . $row["channel_code"] . "</li>"; 
						echo "<li>AgentCode = " . $row["agent_code"] . "</li>"; 
						echo "<li>Sales Vat = " . $row["sales_vat"] . "</li>"; 
						echo "<li>Sales Non Vat = " . $row["sales_non_vat"] . "</li>"; 
						
						echo "<li>VATs Amount = " . $row["vats_amount"] . "</li>"; 
						echo "<li>Coupon No. = " . $row["coupon_no"] . "</li>"; 
						echo "<li>Coupon Amount = " . $row["coupon_amount"] . "</li>"; 
						echo "<li>ShippingFee = " . $row["shipping_fee"] . "</li>"; 
						echo "<li>Shipping Charge  Discount Amount = " . $row["shipping_charge_ciscount_amount"] . "</li>"; 
						
						echo "<li>Packageing Fee = " . $row["packageing_fee"] . "</li>"; 
						echo "<li>Other Charge = " . $row["other_charge"] . "</li>"; 
						echo "<li>TOTAL ORDER Amount = " . $row["total_order_amount"] . "</li>"; 
						echo "<li>CurrencyCode = " . $row["currency_code"] . "</li>"; 
						
						echo "<li>approval_code = " . $row["approval_code"] . "</li>"; 
						echo "<li>masked_pan = " . $row["masked_pan"] . "</li>"; 
						echo "<li>user_defined_1 = " . $row["user_defined_1"] . "</li>"; 
						echo "<li>user_defined_2 = " . $row["user_defined_2"] . "</li>"; 
						echo "<li>user_defined_3 = " . $row["user_defined_3"] . "</li>"; 
						
						echo "<li>user_defined_4 = " . $row["user_defined_4"] . "</li>"; 
						echo "<li>user_defined_5 = " . $row["user_defined_5"] . "</li>"; 
						echo "<li><strong>Store Credit Amount = <u>" . $row["store_credit_amount"] . "</u></strong></li>"; 
						echo "<li>Create Date = " . $row["crs_createddate"] . "</li>"; 
						echo "<li><br/>&nbsp;&nbsp;<a href=\"../autocreditnote.php?ordno="  . $OrderNo . "&crsid=" . $row["batch_id"] . "&process=102\">[Delete CRS]</a></li>";
					echo "</ul>"; 
					echo "<td>";	
				   }
				   
				   echo "</tr></table>";
				}

			?>
		</td>
		<td>&nbsp;&nbsp;</td>
		<td valign="top">
			<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
				<tr>
					<td><strong>ID</strong></td>
					<td><strong>Category</strong></td>
					<td><strong>Oracle Branch Code</strong></td>
					<td><strong>Cost Center Code</strong></td>
					<td><strong>Account Code</strong></td>
					<td><strong>Sub Account Code</strong></td>
					<td><strong>Sale Date</strong></td>
					<td><strong>Debit Amount</strong></td>
					<td><strong>Credit Amount</strong></td>
					<td><strong>Branch Short Name</strong></td>
					<td><strong>Created Date</strong></td>
					<td><strong>BATCH_NO</strong></td>
					<td><strong>#</strong></td>
				</tr>
				<tr>
					<td colspan="13"><strong>Paid before Ship &raquo; Order Approve (paid)</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=1) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>
					</tr>
				   <?php
				   	  $DrAmount1 += $row["gl_debit_amount"];
					  $CrAmount1 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount1, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount1, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				   
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>
				<tr>
					<td colspan="13"><strong>Paid before Ship &raquo; Close The Door</strong></td>
				</tr>
				<?php
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=2) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>		
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>				
					</tr>
				   <?php
				   	  $DrAmount2 += $row["gl_debit_amount"];
					  $CrAmount2 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount2, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount2, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>
				<tr>
					<td colspan="13"><strong>Cash On Delivery &raquo; Close The Door</strong></td>
				</tr>
				<?php
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=3) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>			
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>			
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>		
				<tr>
					<td colspan="13"><strong>Paid before Ship &raquo; Customer Cancel before close the door</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=10) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>		
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>				
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>	
				<tr>
					<td colspan="13"><strong>Paid before Ship &raquo; Customer Cancel After close the door</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=11) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>				
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>		
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>		
				<tr>
					<td colspan="13"><strong>Cash On Delivery - Customer Cancel After close the door</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase=20) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>		
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>				
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>				
				<tr>
					<td colspan="13"><strong>Customer Return</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase in (30,40)) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>			
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>			
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>				
				<tr>
					<td colspan="13"><strong>Customer Refund</strong></td>
				</tr>
				<?php
				$DrAmount1 = 0; $CrAmount1 = 0;
				$DrAmount2 = 0; $CrAmount2 = 0;
				$DrAmount3 = 0; $CrAmount3 = 0;
				
				$query = "Select * From ttackoff_gl Where (gl_orderno='" . mysql_escape_string($OrderNo) . "') and (gl_glcase in (31,41)) Order By gl_id";
				if ($AccArrays = $readConnection->fetchAll($query)) {
				   foreach ($AccArrays as $row) {
				   ?>
					<tr>
						<td align="center"><?php echo $row["gl_id"];?></td>
						<td><?php echo $row["gl_category"];?></td>
						<td><?php echo $row["gl_oracle_branchcode"];?></td>
						<td><?php echo $row["gl_costcenter_code"];?></td>
						<td><?php echo $row["gl_account_code"];?></td>
						<td><?php echo $row["gl_subaccount_code"];?></td>
						<td><?php echo $row["gl_sale_date"];?></td>
						<td align="right"><?php echo $row["gl_debit_amount"];?></td>
						<td align="right"><?php echo $row["gl_credit_amount"];?></td>
						<td><?php echo $row["gl_branch_shortname"];?></td>
						<td><?php echo $row["gl_createddate"];?></td>
						<td><?php echo $row["batch_no"];?></td>		
						<td align="center"><?php echo "<a href=\"deletegl.php?id=". $row["gl_id"] . "&ord=" . $OrderNo . "\">[Delete]</a>";?></td>				
					</tr>
				   <?php
				   	  $DrAmount3 += $row["gl_debit_amount"];
					  $CrAmount3 += $row["gl_credit_amount"];
				   }
				   echo "<tr><td colspan=\"7\">&nbsp;</td>" .
				   			"<td align=\"right\"><strong>" . number_format($DrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td align=\"right\"><strong>" . number_format($CrAmount3, 2, '.', '') . "</strong></td>" . 
							"<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" . 
				   			"</tr>";
				}
				else {
					echo "<tr><td colspan=\"13\" height=\"30\">&nbsp;</td></tr>";
				}
				?>																								
			</table>
		</td>
	</tr>	
</table>


<?php
 }
}
else {
	echo "<p>order not found !!!!!!!</p>";
}
?>
</body>
</html>