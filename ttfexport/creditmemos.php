<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Credit Memos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" src="common.js" type="text/javascript"></script>
<style type="text/css">
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
}
body {
	background-color: #FFF;
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
</style>
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
table.gridtable td.right {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
a:link { text-decoration: none; }
a:hover { text-decoration: underline;}
</style>
</head>

<body>
<?php
// Include Magento application
require_once ( "../app/Mage.php" );
//umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$readConnection = Mage::getSingleton("core/resource")->getConnection("core_read");
date_default_timezone_set('Asia/Bangkok'); 
?>
<form id="tableform" name="tableform" method="get" action=""> 
<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
	<caption>
		<div id="nav_footer">
			<div style="width: 30%; float:left; text-align:left;"> 
				<a href="index.php">[Order List]</a>
			</div>
			<div style="width: 70%; float:right;text-align:right;">
				<?php echo $page_start . " - " . $page_end . " of " . $recordcount . " item(s) || Page(s) : $p of $pagecount";?>
			</div>
		</div>	
	</caption>
	<thead>   	
	<tr>
		<td align="center"><input type="checkbox" name="datC_select" id="datC_select" value="true" onClick="formcheckAll(tableform.dataid,this.checked);"/></td>
		<td align="center"><strong>Credit Memos #</strong></td>
		<td align="center"><strong>Created At</strong></td>
		<td align="center"><strong>Order #</strong></td>
		<td align="center"><strong>Order Date</strong></td>
		<td align="center"><strong>Bill To Name</strong></td>
		<td align="center"><strong>Status</strong></td>
		<td align="center"><strong>Refunded</strong></td>
	</tr>
	</thead>   
	<?php
	$query = "Select * From sales_flat_creditmemo_grid Order By entity_id desc";
	if ($AccArrays = $readConnection->fetchAll($query)) {
	   foreach ($AccArrays as $row) {
	   	$LinkUrl  = "<a href=\"creditmemo_data.php?id=". $row["entity_id"] ."\">";
	   ?>
		<tr>
			<td align="center"><input type="checkbox" name="dataid" id="dataid" value="<?php echo $row["increment_id"];?>" onClick="formcheckOne(tableform.datC_select,tableform.dataid,this);"/></td>
			<td align="center"><?php echo $LinkUrl . $row["increment_id"] . "</a>";?></td>
			<td><?php echo $LinkUrl . $row["created_at"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . $row["order_increment_id"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . $row["order_created_at"] . "</a>";?></td>
			<td><?php echo $LinkUrl . $row["billing_name"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . (((int)$row["state"]==2) ? " Refunded" :  $row["state"]) . "</a>";?></td>
			<td align="right"><?php echo $LinkUrl . number_format($row["grand_total"], 2, ".", ",") . "</a>";?></td>
		</tr>
	   <?php
	   }
	}
	?>
</table>
</form>
</body>
</html>