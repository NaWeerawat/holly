function formcheckAll(pcheckbox,pchecked) {
 if (pcheckbox) {
 	var nbox=pcheckbox.length;
	if (nbox==undefined) { pcheckbox.checked=pchecked} 
	 else {
	    for (var j = 0; j < nbox; j++)  {pcheckbox[j].checked=pchecked;}
	 }	
  }
}

function formcheckOne(pcheckall,pcheckbox,pchecked) {   
    if (pchecked.checked==false) { pcheckall.checked=pchecked.checked; }
	 else {
	     var ibox=pcheckbox.length;
	     for (var j = 0; j < ibox; j++)  {
			if (pcheckbox[j].checked!=pchecked.checked) {	return; }}
			 pcheckall.checked=pchecked.checked;
	 }	 
}


function getformcheckValues(pcheckbox) {
 var xID="";
 if (pcheckbox) {
 	if (pcheckbox.length==undefined) {
		if (pcheckbox.checked==true) { xID=pcheckbox.value}	
	} else
	 {
	  var nbox=pcheckbox.length; 
 	   for (var j = 0; j < nbox; j++)  {
		  if (pcheckbox[j].checked==true) {
		 		 if (xID=="") {	xID=pcheckbox[j].value; } else {	xID=xID +"," +pcheckbox[j].value;}	
			} /*Box Select */
		} /*for loop*/
	 } /*>1*/
 } 
 return xID;	
}

function selectboxChange(yourselectbox){
	if (yourselectbox.selectedIndex >=1) {yourselectbox.form.submit();}
}

function popupCenter(yoururl, dialog_name,dialog_width,dialog_height) 
{
 self.name=="opener";
 var str = "height=" + dialog_height + ",innerHeight=" + dialog_height;
  str += ",width=" + dialog_width + ",innerWidth=" + dialog_width;
  if (window.screen) { var ah = screen.availHeight - 30;
    var aw = screen.availWidth - 10;
    var xc = (aw - dialog_width) / 2; var yc = (ah - dialog_height) / 2;
    str += ",left=" + xc + ",screenX=" + xc;
    str += ",top=" + yc + ",screenY=" + yc;
  } 
  remote=open(yoururl, dialog_name, str);
  remote.focus(); 
}

function confirmDialog(yourUrl){
  popupCenter(yourUrl,"confirmDlg",400,250);
}

function selectmoreDialog(pbox,purl,pQuerystring,pNoselected) {
  var xValue=getformcheckValues(pbox);
  if (xValue=="") {alert(pNoselected);}
  else {
       if (purl.indexOf("?")==-1) {
	   		window.location = purl+"?"+pQuerystring+"="+xValue; 
		} else 
		 {
		 	window.location = purl+"&"+pQuerystring+"="+xValue; 
		 }
   }	
}

//Constant Value
var maxText=65535; //MS ACCESS Max Memo
//Email Address
function isEmailFormat(emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var MsgErr="";   
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	MsgErr=MsgErr+"Email address seems incorrect (check @ and .'s) ";
	return MsgErr;
}
else
{
  var user=matchArray[1]
  var domain=matchArray[2]
  if (user.match(userPat)==null) {
    MsgErr=MsgErr+"The username doesn't seem to be valid. ";
	return MsgErr;
   }
  var IPArray=domain.match(ipDomainPat)
   if (IPArray!=null) {
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        MsgErr=MsgErr+"Destination IP address is invalid ! ";
			return MsgErr;
	     }//IF
		}//for
     }//IP Array
   var domainArray=domain.match(domainPat)
   if (domainArray==null) {
	 MsgErr=MsgErr+"The domain name doesn't seem to be valid.";
	 return MsgErr;
     }
    var atomPat=new RegExp(atom,"g")
    var domArr=domain.match(atomPat)
    var len=domArr.length
    if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3) {
     MsgErr=MsgErr+"The address must end in a three-letter domain, or two letter country.";
	 return MsgErr;
    }
   if (len<2) {
     MsgErr=MsgErr+"This address is missing a hostname!";
	 return MsgErr;
   } 
 }//End  Match
return "0";
}

function EmailValid(emailStr, Requirement) {
  var MsgErr="0"; 	
  if (!((Requirement==false) && (emailStr.length<1))) {
  	MsgErr = isEmailFormat(emailStr);}; 	
  return MsgErr;
}

//Numeric Format
function numFormat(elem, lead, sep, Min,Max)
{
	if (elem.value == '') 
	{
		elem.value = format('0', lead, sep);
		return true;
	}	
	var value = parseInt(cleanNumber(elem.value), 10);
	if (lead == '') 
	{
		if (Min > value)
		{
			alert('You have exceeded the range for the interior size.\nPlease check your information and try again.');
			value = Min;
		}		
		if (value > Max) 
		{
			alert('You have exceeded the range for the interior size.\nPlease check your information and try again.');
			value = Max;
		}
	}
	if (isNaN(value)) 
	{
		alert('You have entered an incorrect character on this field. \nPlease check your information and try again.');
		elem.value = format(Min, lead, sep);
		elem.focus();
		return false;
	}
	elem.value = format(value, lead, sep);
	return true;
}

function format(value, lead, sep)
{
	var strValue = new String(value);
	var len = strValue.length;
	var n;
	var strRet = '';
	var ctChar = 3 - (len%3);
	if (ctChar == 3) ctChar =0;
	for (n=0; len > n; n++) {
		if (ctChar == 3) {
			strRet += sep;
			ctChar = 0;
		}
		ctChar++;
		strRet += strValue.substring(n,n+1)		
	}
	if (lead == '%') 
	{
		return strRet + lead;
	}
	else 
	{
		return lead + strRet;
	}
}

function replace(szBuf, szFind, szReplace, lStart)
{
	var lFind = 0;
	if (!lStart) lStart = 0;
	
	while (lFind != -1) {
		lFind = szBuf.indexOf(szFind, lStart);

		if (lFind != -1) {
			szBuf = szBuf.substring(0,lFind) + szReplace + szBuf.substring(lFind + szFind.length);
			lStart = lFind + szReplace.length;
		}
	}
	return szBuf;
}

function cleanNumber(strNum)
{
	if (!strNum) return strNum;
	strNum = replace(strNum, '$', '', 0);
	strNum = replace(strNum, ',', '', 0);
	strNum = replace(strNum, '%', '', 0);	
	return strNum;
}

function onIntRangeChange(pElem,Min,Max) { 
	var v =cleanNumber(pElem.value);
	var bInvalid = false;
	if (isNaN(Number(v))) {
		bInvalid = true;}	
		numFormat(pElem, '', '',Min,Max);
	return(true);
}

//�ѧ��������Ѻ�礹��ʡ�Ţͧ���--
function getFileExt(fFile) {
	var myext="";
	myext=fFile.substr(fFile.lastIndexOf(".")+1,(fFile.length-fFile.lastIndexOf(".")+1));
	return myext;
}

//�ѧ��������ǡѺ�ѹ���
function DateBoxValid(fDay, fMonth, fYear, fRequest)	{
  //return 0 If  Data is in Currect Format and the value is possible.
  var DateField =  fMonth.options[fMonth.selectedIndex].value + "/" + fDay.options[fDay.selectedIndex].value + "/"+  fYear.options[fYear.selectedIndex].value;
  var dateFormat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
  var matchArray = DateField.match(dateFormat); 
  var ErrMessage="";
    if (matchArray == null) {
		  if ( fRequest==true) { ErrMessage=" empty Date value, ";}  
     }
	  else //Check data Value
       {	 
	    day = matchArray[3];
    	month = matchArray[1]; 
		year = matchArray[4];
		if (day < 1 || day > 31) {
			ErrMessage=ErrMessage+" Day must be between 1 and 31, ";}
	  	if (month < 1 || month > 12) {
			ErrMessage=ErrMessage+ " Month must be between 1 and 12, ";}
	  	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
			ErrMessage=ErrMessage+" - Month "+month+" doesn't have 31 days!, ";}
	  	if (month == 2) {
	    	 var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
         	  if (day>29 || (day==29 && !isleap)) {
                 ErrMessage=ErrMessage+" - February " + year + " doesn't have " + day + " days!, "; }
	       } 		   
	 }//end else null
	 if ((ErrMessage.length>0) ||  (DateField.length ==0)) {
	    if (DateField.length==0) { 
		  return " must have value.";
		 } else { 
			  return ErrMessage;
			}  
	  }  else
	    {

			return "";
	  }
}//End function

function checkBoxs(pbox) {
 var xID="";
 if (pbox) {
	  var nbox=pbox.length; 
 	   for (var j = 0; j < nbox; j++)  {
		  if (pbox[j].checked==true) {
		 		 if (xID=="") {	xID=pbox[j].value; } else {	xID=xID +"," +pbox[j].value;}	
			} /*Box Select */
		} /*for loop*/
 } 
 return xID;	
}


function getRadio(pbox) {
 var xID="";
 if (pbox) {
	  var nbox=pbox.length; 
	  if (nbox==1) {
	   
	  } 
	  else {
 	   for (var j = 0; j < nbox; j++)  {
		  if (pbox[j].checked==true) {
		 		 if (xID=="") {	xID=pbox[j].value; } else {	xID=xID +"," +pbox[j].value;}	
			} /*Box Select */
		} /*for loop*/
	 }//nbox=1?	
 } 
 return xID;	
}

function validRequired(formField,aMessage)
{
	var result = true;
	if ((formField.value == "") && (formField.value.length<1)){
	  alert(aMessage);formField.focus();result = false;
	}
	return result;
}

function validMaxRequired(formField,aMax,aMessage)
{
	var result = true;
	if (formField.value.length<aMax) {
		alert(aMessage); formField.focus();	result = false;
	}
	return result;
}

function getformcheckValues(pcheckbox) {
 var xID="";
 if (pcheckbox) {
 	if (pcheckbox.length==undefined) {
		if (pcheckbox.checked==true) { xID=pcheckbox.value}	
	} else
	 {
	  var nbox=pcheckbox.length; 
 	   for (var j = 0; j < nbox; j++)  {
		  if (pcheckbox[j].checked==true) {
		 		 if (xID=="") {	xID=pcheckbox[j].value; } else {	xID=xID +"," +pcheckbox[j].value;}	
			} /*Box Select */
		} /*for loop*/
	 } /*>1*/
 } 
 return xID;	
}

function getformradioValues(pcheckbox) {
 var xID="";
 if (pcheckbox) {
 	if (pcheckbox.length==undefined) {
		if (pcheckbox.checked==true) { xID=pcheckbox.value}	
	} else
	 {
	  var nbox=pcheckbox.length; 
 	   for (var j = 0; j < nbox; j++)  {
		  if (pcheckbox[j].checked==true) {
		 		 if (xID=="") {	xID=pcheckbox[j].value; } else {	xID=xID +"," +pcheckbox[j].value;}	
			} /*Box Select */
		} /*for loop*/
	 } /*>1*/
 } 
 return xID;	
}