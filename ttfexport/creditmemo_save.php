<?php
// Include Magento application
require_once ( "../app/Mage.php" );
//umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$readConnection = Mage::getSingleton("core/resource")->getConnection("core_read");
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$CreditMemoID = $_POST["id"];
$TransferType = $_POST["order_refund_type"];
if ($TransferType=="eft") {
	$TransferBank = $_POST["order_ebt_acc_bank"];
	$TransferAccName = $_POST["order_ebt_acc_name"];
	$TransferAccNo = $_POST["order_ebt_acc_number"];
}
else {
	$TransferBank = "";
	$TransferAccName = "";
	$TransferAccNo = "";
}

$TakeOffGLSql = "Update sales_flat_creditmemo Set " . 
					"ttf_refundtype='" . mysql_escape_string($TransferType) . "' " .
					",ttf_refundbank='" . mysql_escape_string($TransferBank) . "' " .
					",ttf_refundaccountno='" . mysql_escape_string($TransferAccNo) . "' " .
					",ttf_refundaccountname='" . mysql_escape_string($TransferAccName) . "' " .
					" Where entity_id=" . (int)$CreditMemoID;
$write->query($TakeOffGLSql);

//$cmemo = Mage::getModel("sales/creditmemo");
//$cmemo->loadByIncrementId($_POST["xid"]);

//$cmemo->addComment($TakeOffGLSql, false);


//echo "Test data : " . $cmemo->getIncrementId();
header("Location: creditmemo_data.php?id=" . (int)$CreditMemoID);
die();
?>