<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Untitled</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" src="common.js" type="text/javascript"></script>
<style type="text/css">
body,td,th {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
}
body {
	background-color: #FFF;
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
</style>
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
table.gridtable td.right {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
a:link { text-decoration: none; }
a:hover { text-decoration: underline;}
</style>
</head>

<body>
<?php
// Include Magento application
require_once ( "../app/Mage.php" );
//umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$readConnection = Mage::getSingleton("core/resource")->getConnection("core_read");
date_default_timezone_set('Asia/Bangkok'); 
?>
<form id="tableform" name="tableform" method="get" action=""> 
<table width="100%" cellpadding="1" cellspacing="1" border="0" class="gridtable">
	<caption>
		<div id="nav_footer">
			<div style="width: 30%; float:left; text-align:left;"> 
 				<div id="button-box">
					<input type="button" name="ExportCRS" value="Export CRS" onClick='selectmoreDialog(tableform.dataid, "export_crs.php", "id","Can not export data !!\n\nPlease select one or more items and try again.");'>
					<input type="button" name="ExportGL" value="Export GL" onClick='selectmoreDialog(tableform.dataid, "export_gl.php", "id","Can not export data !!\n\nPlease select one or more items and try again.");'>
		    	</div> 
			</div>
			<div style="width: 70%; float:right;text-align:right;">
				<?php echo $page_start . " - " . $page_end . " of " . $recordcount . " item(s) || Page(s) : $p of $pagecount";?>
			</div>
		</div>	
	</caption>
	<thead>   	
	<tr>
		<td align="center"><input type="checkbox" name="datC_select" id="datC_select" value="true" onClick="formcheckAll(tableform.dataid,this.checked);"/></td>
		<td><strong>RMA #</strong></td>
		<td><strong>Date Requested</strong></td>
		<td><strong>Status</strong></td>
		<td><strong>Order #</strong></td>
		<td><strong>Order Date</strong></td>
		<td><strong>Customer Name</strong></td>
	</tr>
	</thead>   
	<?php
	$query = "Select * From enterprise_rma_grid Order By entity_id desc";
	if ($AccArrays = $readConnection->fetchAll($query)) {
	   foreach ($AccArrays as $row) {
	   	$LinkUrl  = "<a href=\"rma_data.php?rmaid=". $row["entity_id"] ."\">";
	   ?>
		<tr>
			<td align="center"><input type="checkbox" name="dataid" id="dataid" value="<?php echo $row["increment_id"];?>" onClick="formcheckOne(tableform.datC_select,tableform.dataid,this);"/></td>
			<td align="center"><?php echo $LinkUrl . $row["increment_id"] . "</a>";?></td>
			<td><?php echo $LinkUrl . $row["date_requested"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . $row["status"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . $row["order_increment_id"] . "</a>";?></td>
			<td align="center"><?php echo $LinkUrl . $row["order_date"] . "</a>";?></td>
			<td><?php echo $LinkUrl . $row["customer_name"] . "</a>";?></td>
		</tr>
	   <?php
	   }
	}
	?>
</table>
</form>
</body>
</html>