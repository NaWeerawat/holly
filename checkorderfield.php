<?php
// Include Magento application
require_once ( "app/Mage.php" );
umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$OnetwothreeOrderNo = $_GET["ordno"];
$order = Mage::getModel('sales/order');
$order->loadByIncrementId($OnetwothreeOrderNo);
// a more complete example
echo "<p>". date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())) ."</p>";

echo "<ul>";
	echo "<li>getPbsPaidcompeteGl=" . $order->getPbsPaidcompeteGl() . "</li>";
	echo "<li>getPbsShippedGl=" . $order->getPbsShippedGl() . "</li>";
	echo "<li>getCodShippedGl=" . $order->getCodShippedGl() . "</li>";
	echo "<li>getPbsPaidcompeteCsr=" . $order->getPbsPaidcompeteCsr() . "</li>";
	echo "<li>getCodShippedCsr=" . $order->getCodShippedCsr() . "</li>";
	echo "<li>getTtfCrsCancel=" . $order->getTtfCrsCancel() . "</li>";
	echo "<li>getTtfPbsCancelBfdoorGl=" . $order->getTtfPbsCancelBfdoorGl() . "</li>";
	echo "<li>getTtfPbsCancelAfdoorGl=" . $order->getTtfPbsCancelAfdoorGl() . "</li>";
	echo "<li>getTtfCodCancelAfdoorGl=" . $order->getTtfCodCancelAfdoorGl() . "</li>";
	echo "<li>getTtfReturnGl=" . $order->getTtfReturnGl() . "</li>";
	echo "<li>getTtfRefundGl=" . $order->getTtfRefundGl() . "</li>";
echo "</ul>";
?>