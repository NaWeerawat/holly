<?php
// Include Magento application
require_once ( "app/Mage.php" );
umask(0);
//load Magento application base "default" folder
$app = Mage::app("default");
$OnetwothreeOrderNo = $_GET["ordno"];
$order = Mage::getModel('sales/order');
$order->loadByIncrementId($OnetwothreeOrderNo);


switch ($_GET["process"]) {
	
	case "100" :
		echo "<h4>PBS - Create CRS - Payment Completed</h4>";
		createTTakeOffCRS($order);
		echo "<p><a href=\"/ttfexport/viewdata.php?ordno=". $OnetwothreeOrderNo ."\">&raquo;&raquo; Back To TTF Data</a></p>";
        break;
	case "101" :
		echo "<h4>PBS - Create GL - Payment Completed</h4>";
		createTTakeOffCL_PBSOrderApprove($order);
		echo "<p><a href=\"/ttfexport/viewdata.php?ordno=". $OnetwothreeOrderNo ."\">&raquo;&raquo; Back To TTF Data</a></p>";
        break;
	case "102" :
		echo "<h4>Delete CRS : " . $_GET["crsid"] ."</h4>";
		DeleteCRS($_GET["crsid"]);
		echo "<p><a href=\"/ttfexport/viewdata.php?ordno=". $OnetwothreeOrderNo ."\">&raquo;&raquo; Back To TTF Data</a></p>";
        break;		
    case "0":
        //=> Create Credit Note & CRS
		echo "<h4>Create CRS - Cancel</h4>";		
		createTTakeOffCRS_BSBCalcel($order);
        break;		
    case "1":
        //=> Create Credit Note & CRS
		echo "<h2>Can create memo</h2>";
		
		//=> ******************************
		if ($order->canCreditmemo()) {
	
					$data = array();
			
			         
			        $service = Mage::getModel('sales/service_order', $order);
			        
			        $creditmemo = $service->prepareCreditmemo($data);
			
			        // refund to Store Credit
			        if ($refundToStoreCreditAmount) {
			            // check if refund to Store Credit is available
			            if ($order->getCustomerIsGuest()) {
			                $this->_fault('cannot_refund_to_storecredit');
			            }
			            $refundToStoreCreditAmount = max(
			                0,     min($creditmemo->getBaseCustomerBalanceReturnMax(), $refundToStoreCreditAmount)
			            );
						
			            if ($refundToStoreCreditAmount) {
			                $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice($refundToStoreCreditAmount);
			                $creditmemo->setBaseCustomerBalanceTotalRefunded($refundToStoreCreditAmount);
			                $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice(
			                    $refundToStoreCreditAmount*$order->getStoreToOrderRate()
			                );
			                // this field can be used by customer balance observer
			                $creditmemo->setBsCustomerBalTotalRefunded($refundToStoreCreditAmount);
			                // setting flag to make actual refund to customer b<p style="line-height: 100%"></p>alance after credit memo save
			                $creditmemo->setCustomerBalanceRefundFlag(true);
			            }
			        }
			        $creditmemo->setPaymentRefundDisallowed(true)->register();
			        // add comment to creditmemo
			        if (!empty($comment)) {
			            $creditmemo->addComment($comment, $notifyCustomer);
			        }
			        try {
			            Mage::getModel('core/resource_transaction')
			                ->addObject($creditmemo)
			                ->addObject($order)
			                ->save();
			            // send email notification
			            $creditmemo->sendEmail($notifyCustomer, ($includeComment ? $comment : ''));
			        } catch (Mage_Core_Exception $e) {
			            $this->_fault('data_invalid', $e->getMessage());
			        }
			        
					
					echo " > Credit Memo No : " . $creditmemo->getIncrementId();	
					
					echo "<h4>Create CRS</h4>";		
					
					createTTakeOffCRS_BSBCalcel($order);
		}
		else {
			echo "<h4>Can not create credit memo</h4>";
		}
		
        break;
    case "2":
        //=> PBS - Create GL - Shipment_cancel_in_progress
		echo "<h4>PBS - Create GL - Shipment_cancel_in_progres</h4>";
		createTTakeOffGL_PbsShipmentCancelInProgress($order);
        break;
    case "3":
        //PBS - Create GL - Customer Cancel After close the door
		echo "<h4>PBS - Create GL - Customer Cancel After close the door</h4>";
		createTTakeOffGL_PbsShipmentCancelAfterCloseDoor($order);
        break;
    case "4":
        //COD - Create GL -Customer Cancel After close the door
		echo "<h4>COD - Create GL -Customer Cancel After close the do</h4>";
		createTTakeOffGL_CODCancelAfterCloseDoor($order);
		
        break;		
    case "5":
       //Create GL - Customer Return
		echo "<h4>Create GL -> Return Only</h4>";
		createTTakeOffGL_Return($order);
		
        break;		
    case "6":
       //Create GL -Customer Refund
		echo "<h4>Create GL -> Refund Only</h4>";
		createTTakeOffGL_Refund($order);
		
        break;		
		
	case "7" :
		echo "<h4>Create GL -> PBS Close The Door Only</h4>";
		createTTakeOffCL_PBSCloseTheDoor($order);
		
		break;		
}





echo "<p><a href=\"/ttfexport/viewdata.php?ordno=". $OnetwothreeOrderNo ."\">&raquo;&raquo; Back To TTF Data</a></p>";




/*********************************************************************************************/
//=> PART 2  - ��� Cancel �ͧ PBS 
/*********************************************************************************************/
function createTTakeOffCRS_BSBCalcel($order) {
		Mage::log("Create CRS - PBS - Cancel", null, 'mgf.log');
		try 
		{
			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
			Mage::log(" Order -> Start Create Cancel (CSR) : " . $order->getCancelCsr(), null, 'mgf.log');
			if ($order->getCancelCsr() !="Y") {
				//=> Start Create *******************************
				$storeId = Mage::app()->getStore()->getId();
				$OrderAmount = $order->getBaseGrandTotal();
				$OrderStoreCredit = abs($order->getCustomerBalanceAmount());
				
				//=> Get Data
				$crsBatch_No = "";
				$csrStore_No = trim(Mage::getStoreConfig('ttfglcrs/default/srcstroreno', $storeId));
				$csrOrder_Date = Mage::getModel('core/date')->date('d/m/Y', strtotime($order->getCreatedAtFormated('full')));
				$csrOrder_Time = Mage::getModel('core/date')->date('His', strtotime($order->getCreatedAtFormated('full')));
				$csrOrder_Type = "C";
				$RefundType = strtolower(trim($order->getPbsRefundType()));
				
				$csrMerchant_Id = "???"; 
				
				$csrOrder_Id = $order->getIncrementId();
				
				$csrInvoice_Date = "";
				$csrInvoice_No = "";
				if ($order->hasInvoices()) {
				    foreach ($order->getInvoiceCollection() as $inv) {
				        $csrInvoice_No = $inv->getIncrementId();
						$csrInvoice_Date =  Mage::getModel('core/date')->date('d/m/Y', strtotime($inv->getCreatedAt()));
				    } 
				}
				
				$csrCredit_Note_No = "";
				$csrCredit_Note_Date = "";
				
				if ($order->hasCreditmemos()) {
				    foreach ($order->getCreditmemosCollection() as $creditmemo) {
				        $csrCredit_Note_No = $creditmemo->getIncrementId();
						$csrCredit_Note_Date =  Mage::getModel('core/date')->date('d/m/Y', strtotime($creditmemo->getCreatedAt()));
				    } 
				}				
				
				if ($order->getPaymentModel()=="PBS") {
					$csrPayment_Model = "PBS";
				} else {
					$csrPayment_Model = "COD";
				}
				
				
				$csrAgent_Code = ""; 
				
				//=> Get VAT or None Vat			
				$csrSales_Vat = 0;
				$csrSales_NonVat = 0;
				$ItemVATAmount = 0; $ItemNonVATAmount = 0;
				//$product_model = Mage::getModel('catalog/product');
	        	$items = $order->getAllItems();
				foreach($items as $itemId => $item)
	        	{
					if ($item->getTaxAmount()==0) {
						$ItemNonVATAmount += $item->getRowTotal();
					}
					else {
						$ItemVATAmount += $item->getRowTotal();
					}
				
					//$_product = $product_model->load($item->getProductId()); // getting product details here to get description
					//$ItemsData .= $item->getItemId() . "|" . $item->getSku() . "|" . $item->getQtyOrdered() . "|". $item->getName() . "|". $_product->getData("cost_center") . "\n\r";
				
				} 
				unset($items);			
				$csrSales_Vat = -1 * $ItemVATAmount;
				$csrSales_NonVat = -1 * $ItemNonVATAmount;
				$csrVATsAmount = -1 * $order->getTaxAmount();
				
				//> ������ͧ��� ������������������
				$csrCouponNo = $order->getCouponCode();
				$csrCouponAmount = abs($order->getDiscountAmount());
				
				$csrShippingFee = -1 * $order->getShippingAmount();
				$csrShippingCharge_DiscountAmount = 0;
				$csrPackageingFee = 0;
				$csrOtherCharge = 0;
				
				$csrTotalOrderAmount = -1 * $order->getGrandTotal();
				$csrCurrencyCode =  $order->getOrderCurrencyCode();
				
				$csrApprovalCode = "000000";
				$csrMasked_Pan = "0";
				
				
				//=> Broker Code by Refund information
				switch ($RefundType) {
					case "storecredit":
						$csrBroker_ID = "ECOM";
						$csrPayment_Channel = "004";
						$csrMerchant_Id = "99998"; 
						if ($OrderAmount>0) {
							$OrderStoreCredit = $OrderStoreCredit + $OrderAmount;
							$csrTotalOrderAmount = 0;
						}
						break;
				    case "cash":
				        $csrBroker_ID = "CASH";
						$csrPayment_Channel = "004";
						$csrChannel_Code = "EFT";
						$csrMerchant_Id = "99999"; 
				        break;
				    case "eft":
				        $csrBroker_ID = "ECOM";
						$csrPayment_Channel = "004";
						$csrChannel_Code = "EFT";
						$csrMerchant_Id = "99997"; 
				        break;
				    case "2c2p":
				        $csrBroker_ID = "2C2P";
						$csrPayment_Channel = "001";
						$csrChannel_Code = $order->getChannelCode();
						$csrMerchant_Id = $order->getMerchantId();
				        break;
					default:
						//=> Return To Carrier
						$OrderShipCarrierCode = trim($order->getCadpShipCarrier());
						$CarrierCode = $OrderShipCarrierCode;
						switch ($OrderShipCarrierCode) {
						    case "4014":
						        $CarrierCode = "DHL";
						        break;
						    case "4015":
						        $CarrierCode = "KERRY";
						        break;
					        case "4001":
					        	$CarrierCode = "DHL";
					        	break;
						}		
						
						$csrPayment_Model = "COD";
						$csrPayment_Channel = $CarrierCode;
						$csrBroker_ID = $CarrierCode;
						$csrMerchant_Id = $CarrierCode;
						
						break;
				}
				
				$CRSFieldSQL  = "Insert Into ttakeoff_csr(" .
							"crs_createddate,batch_no,strore_no,order_date,order_time,order_type, broker_id,invoice_date,merchant_id,order_id,invoice_no," .
							"credit_note_no,credit_note_date,payment_model,payment_channel,channel_code,agent_code," .
							"sales_vat,sales_non_vat,vats_amount,coupon_no,coupon_amount,shipping_fee,shipping_charge_ciscount_amount,packageing_fee," .
							"other_charge, total_order_amount, currency_code, approval_code, masked_pan, " . 
							"user_defined_1, user_defined_2, user_defined_3, user_defined_4, user_defined_5, " .
							"store_credit_amount " . 
						 ") Values(";
				
				//=> Write to SQL
				$TakeOffCrsSql .= $CRSFieldSQL  . 
						"NOW()," . 
					 	"'" . mysql_escape_string($crsBatch_No) . "'," . 
					 	"'" . mysql_escape_string($csrStore_No) . "'," . 
					 	"'" . mysql_escape_string($csrOrder_Date) . "'," . 
					 	"'" . mysql_escape_string($csrOrder_Time) . "'," . 
					 	"'" . mysql_escape_string($csrOrder_Type) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($csrBroker_ID) . "'," . 
					 	"'" . mysql_escape_string($csrInvoice_Date) . "'," . 
					 	"'" . mysql_escape_string($csrMerchant_Id) . "'," . 
					 	"'" . mysql_escape_string($csrOrder_Id) . "'," . 
					 	"'" . mysql_escape_string($csrInvoice_No) . "'," . 
						"" .  //=> End Row 1
					 	"'" . mysql_escape_string($csrCredit_Note_No) . "'," . 
					 	"'" . mysql_escape_string($csrCredit_Note_Date) . "'," . 
					 	"'" . mysql_escape_string($csrPayment_Model) . "'," . 
					 	"'" . mysql_escape_string($csrPayment_Channel) . "'," . 
					 	"'" . mysql_escape_string($csrChannel_Code) . "'," . 
						"'" . mysql_escape_string($csrAgent_Code) . "'," . 
						"" . //=> End Row 2
					 	$csrSales_Vat . "," . 
						$csrSales_NonVat . "," . 
						$csrVATsAmount . "," . 
					 	"'" . mysql_escape_string($csrCouponNo) . "'," . 
						$csrCouponAmount . "," . 
						$csrShippingFee . "," . 
						$csrShippingCharge_DiscountAmount . "," . 
						$csrPackageingFee . "," . 
						"" . //=> End Row 3
						$csrOtherCharge . "," . 
						$csrTotalOrderAmount . "," . 
					 	"'" . mysql_escape_string($csrCurrencyCode) . "'," . 
					 	"'" . mysql_escape_string($csrApprovalCode) . "'," . 
					 	"'" . mysql_escape_string($csrMasked_Pan) . "'," . 
						"" . //=> End Row 4
					 	"'" . mysql_escape_string($csrUser_defined_1) . "'," . 
					 	"'" . mysql_escape_string($csrUser_defined_2) . "'," . 
					 	"'" . mysql_escape_string($csrUser_defined_3) . "'," . 
					 	"'" . mysql_escape_string($csrUser_defined_4) . "'," . 
					 	"'" . mysql_escape_string($csrUser_defined_5) . "', " . 
						$OrderStoreCredit . "" . 
					 ")";
				$write->query($TakeOffCrsSql);
				//=> End Write SQL


				Mage::log(" " . $order->getPbsPaidcompeteCsr(), null, 'mgf.log');
				//=> Update Status
				Mage::log(" Order Update Fix xxxxxxxxxxxxxxxxx : ", null, 'mgf.log');
				$order->setCancelCsr("Y");
				$order->save();
				//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_csr','Y');
				Mage::log(" Order Update Fix yyyyyyyyyyyyyyyyyyyyy : ", null, 'mgf.log');
				//=> End Create ************************
			}
			else {
				Mage::log(" ----- Exist CRS ********************", null, 'mgf.log');   
			}
		} catch (Exception $e) { //Exception $e) {
			Mage::log((string)$e, null, 'mgf.log');   
		}
}

function createTTakeOffGL_PbsShipmentCancelInProgress($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- 1. Create GL -> PSB shipment_cancel_in_progress -xxxxxxxxxxxxxxxxxxxxxxxx---------", null, 'mgf.log');   
		//if ($order->getPbsPaidcompeteGl() !="Y") {
			//=> Build GL Data
			Mage::log("--- GL - Build", null, 'mgf.log');
			$glCategory = "Cancel-PBS-B";//"Sale-PBS";
			$glOracleBranchCode ="035002";
			$glBranchShortName = "ECOM01";

			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',10,NOW()";
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');

			//=> Step 1. Product VAT / Non VAT
			//=> 1= Suspense Sales In transit VAT, 1 = ��駾ѡ����Թ���Ẻ��VAT
			//=> 2= Suspense Sales In transit Non VAT, 2 = ��駾ѡ����Թ���Ẻ��Non VAT

			$product_model = Mage::getModel('catalog/product');
	        
			$items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$glDebitAmount = 0;
				$glCreditAmount =0;
				
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=2) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=1) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				$glDebitAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
				
				
			} 
			unset($items);
			
			//=> 2. 3=Suspense Installation / ��駾ѡ��ҵԴ���
			
			//=> 3. 4=Suspense Packaging Fee / ��駾ѡ�������Թ���
			
			//=> 4. 5=Suspense Service / ��駾ѡ��Һ�ԡ������
			
			//=> 5. 6=Suspense Shipping Fee / ��駾ѡ��ҨѴ���Թ���
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $ShippingAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=6) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			
			//=> 6. 7=Suspense Trade Discount
			
			//=> 7. 8 = Selling VAT / VAT �ͧ��â��
			$VATAmount = $order->getTaxAmount();
			if ($VATAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $VATAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=8) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Vat Amount >0 = End
			}
			
			
			//=> Part 2 - Payment ******************/
			$glCategory = "Cancel-PBS-B";
			$RefundType = strtolower(trim($order->getPbsRefundType()));
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			//=> 
			
			//=> 1 = Credit and debit card
			if ($RefundType=="2c2p") {
				//=> 
				$OrdPaymentType = trim($order->getPaymentChannel());
				$OrdAgentChannel = trim($order->getChannelCode()) . "|" . trim($order->getAgentCode());
				
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 
				$query = "Select * From ttakeoff_payment Where (accpayment_type='". mysql_escape_string($OrdPaymentType) ."') and (accpayment_issuer_dealer='". mysql_escape_string($OrdAgentChannel) ."') " . 
							" and (accpayment_no=1) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				//echo $query;
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database
				//=> End
			}			
			
			//=> 2=Store Credit
			if (($RefundType=="storecredit") || ($OrderStoreCredit != 0)) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				if ($RefundType=="storecredit") {
					$glCreditAmount = $OrderAmount + $OrderStoreCredit;
				}
				else {
					$glCreditAmount =$OrderStoreCredit;
				}
				
				$query = "Select * From ttakeoff_payment Where (accpayment_no=2) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End
			}
			//-> Store Credit
			
			//=> 3=Transfer cash to Customer(EFT)
			if ($RefundType=="eft") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=3) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End				
			}
			//-> Transfer cash to Customer(EFT)
			
			//=> 4=cash
			if ($RefundType=="cash") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=4) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End					
			}
			//-> cash
			
			
			
			
		
			

			//=> Payment 
			
				//=> 5. Supplier discount
				
				//=> 6.Suspense Cash discount of Promotion
				if ($OrderDiscount>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = 0;
					$glCreditAmount = $OrderDiscount; 
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_no=6) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
						}
					}
					//=> 
					//=> Post GL Table 
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					$write->query($TakeOffGLSql);
					//=> End - Post to GL
				}
				
				

			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			//Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			//$order->setPbsPaidcompeteGl("Y");
			//$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			//Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		//else {
		//	Mage::log("--- GL - Exist", null, 'mgf.log');
		//}
		//=>
	}
	


function createTTakeOffGL_PbsShipmentCancelAfterCloseDoor($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- 1. Create GL -> Customer Cancel After close the door -xxxxxxxxxxxxxxxxxxxxxxxx ---------", null, 'mgf.log');   
		//if ($order->getPbsPaidcompeteGl() !="Y") {
			//=> Build GL Data
			Mage::log("--- GL - Build", null, 'mgf.log');
			$glCategory = "Cancel-PBS-A";//"Sale-PBS";
			$glOracleBranchCode ="035002";
			$glBranchShortName = "ECOM01";

			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',11,Now()";
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');

			//=> Step 1. Product VAT / Non VAT
			//=> 1= ����Թ���Ẻ��VAT
			//=> 2= ����Թ���Ẻ��Non VAT

			$product_model = Mage::getModel('catalog/product');
	        
			$items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$glDebitAmount = 0;
				$glCreditAmount =0;
				
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=2) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=1) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				
				$glDebitAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
			} 
			unset($items);
			
			//=> 2. 3= ��ҵԴ���
			
			//=> 3. 4= �������Թ���
			
			//=> 4. 5= ��Һ�ԡ������
			
			//=> 5. 6= ��ҨѴ���Թ���
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $ShippingAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=6) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			
			//=> 6. 7=Suspense Trade Discount
			
			//=> 7. 8 = Selling VAT / VAT �ͧ��â��
			$VATAmount = $order->getTaxAmount();
			if ($VATAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $VATAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=9) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Vat Amount >0 = End
			}
			
			
			//=> Part 2 - Payment ******************/
			$glCategory = "Cancel-PBS-A";
			$RefundType = strtolower(trim($order->getPbsRefundType()));
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			//=> 
			
			//=> 1 = Credit and debit card
			if ($RefundType=="2c2p") {
				//=> 
				$OrdPaymentType = trim($order->getPaymentChannel());
				$OrdAgentChannel = trim($order->getChannelCode()) . "|" . trim($order->getAgentCode());
				
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 
				$query = "Select * From ttakeoff_payment Where (accpayment_type='". mysql_escape_string($OrdPaymentType) ."') and (accpayment_issuer_dealer='". mysql_escape_string($OrdAgentChannel) ."') " . 
							" and (accpayment_no=1) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				//echo $query;
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database
				//=> End
			}			
			
			//=> 2=Store Credit
			if (($RefundType=="storecredit") || ($OrderStoreCredit != 0)) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				if ($RefundType=="storecredit") {
					$glCreditAmount = $OrderAmount + $OrderStoreCredit;
				}
				else {
					$glCreditAmount =$OrderStoreCredit;
				}
				
				$query = "Select * From ttakeoff_payment Where (accpayment_no=2) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End
			}
			//-> Store Credit
			
			//=> 3=Transfer cash to Customer(EFT)
			if ($RefundType=="eft") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=3) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End				
			}
			//-> Transfer cash to Customer(EFT)
			
			//=> 4=cash
			if ($RefundType=="cash") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=4) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End					
			}
			//-> cash
			
			
			
			
		
			

			//=> Payment 
			
				//=> 5. Supplier discount
				
				
				//=> 6.Suspense Cash discount of Promotion
				if ($OrderDiscount>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = 0;
					$glCreditAmount = $OrderDiscount; 
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_no=6) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
						}
					}
					//=> 
					//=> Post GL Table 
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					$write->query($TakeOffGLSql);
					//=> End - Post to GL
				}
				
				

			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			//Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			//$order->setPbsPaidcompeteGl("Y");
			//$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			//Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		//else {
		//	Mage::log("--- GL - Exist", null, 'mgf.log');
		//}
		//=>
	}
	
	//*************** PATH 3 ***********************
	//   Cancel COD  -Customer Cancel After close the door
		
function createTTakeOffGL_CODCancelAfterCloseDoor($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- 1. Create GL -> COD - Customer Cancel After close the door -xxxxxxxxxxxxxxxxxxxxxxxx ---------", null, 'mgf.log');   
		//if ($order->getPbsPaidcompeteGl() !="Y") {
			//=> Build GL Data
			Mage::log("--- GL - Build", null, 'mgf.log');
			$glCategory = "Cancel-COD";//"Sale-PBS";
			$glOracleBranchCode ="035002";
			$glBranchShortName = "ECOM01";

			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',20,Now()";
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');

			//=> Step 1. Product VAT / Non VAT
			//=> 1= ����Թ���Ẻ��VAT / Sale Return VAT
			//=> 2= ����Թ���Ẻ��Non VAT / Sale Return Non VAT

			$product_model = Mage::getModel('catalog/product');
	        
			$items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$glDebitAmount = 0;
				$glCreditAmount =0;
				
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=2) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=1) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				$glDebitAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
				
				
			} 
			unset($items);
			
			//=> 2. 3= ��ҵԴ��� / Installation
			
			//=> 3. 4= �������Թ��� / Packaging Fee
			
			//=> 4. 5= ��Һ�ԡ������ / Service
			
			//=> 5. 6= ��ҨѴ���Թ��� / Shipping Fee
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $ShippingAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=6) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			
			//=> 6. 7=Trade Discount  VAT
			
			//=> 6. 8=Trade Discount  Non VAT
			
			
			//=> 7. 9 = Selling VAT / VAT �ͧ��â��
			$VATAmount = $order->getTaxAmount();
			if ($VATAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $VATAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=9) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Vat Amount >0 = End
			}
			
			
			//=> Part 2 - Payment ******************/
			$glCategory = "Cancel-COD";
			$ShipCarrierCodeType = trim($order->getCadpShipCarrier());
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			//=> 
			
			
			//=> 1 = Credit and debit card
			if ($OrderAmount>0) {
				//=> 
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 
				$query = "Select * From ttakeoff_payment Where (accpayment_issuer_dealer='". mysql_escape_string($ShipCarrierCodeType) ."') " . 
							" and (accpayment_no=1) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				//echo $query;
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				} 
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database
				//=> End
			}			
			
			//=> 4=Store Credit
			if ($OrderStoreCredit != 0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount =$OrderStoreCredit;
				
				$query = "Select * From ttakeoff_payment Where (accpayment_no=4) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End
			}
			//-> Store Credit
			
				//=> 2. Supplier discount
				
				
				//=> 3.Suspense Cash discount of Promotion
				if ($OrderDiscount>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = 0;
					$glCreditAmount = $OrderDiscount; 
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_no=3) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
						}
					}
					//=> 
					//=> Post GL Table 
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					$write->query($TakeOffGLSql);
					//=> End - Post to GL
				}
				
				

			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			//Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			//$order->setPbsPaidcompeteGl("Y");
			//$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			//Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		//else {
		//	Mage::log("--- GL - Exist", null, 'mgf.log');
		//}
		//=>
	}	
	
	//*************** PATH 4 ***********************
	// Return - Customer Return - Payback By Credit Card/Transfer - Receipt Goods from Customer at  DC and Account cat HO confirm Refund
		
function createTTakeOffGL_Return($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- 1. Create GL -> Customer Return ---------", null, 'mgf.log');   
		//if ($order->getPbsPaidcompeteGl() !="Y") {
			//=> Build GL Data
			Mage::log("--- GL - Build (Step : Income product VAT / Non VAT)", null, 'gl_return.log');
			$glCategory = "Return";//"Sale-PBS";
			$glOracleBranchCode ="035002";
			$glBranchShortName = "ECOM01";

			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',30,Now()";
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');

			//=> Step 1. Product VAT / Non VAT
			//=> 1= ����Թ���Ẻ��VAT / Sale Return VAT
			//=> 2= ����Թ���Ẻ��Non VAT / Sale Return Non VAT

			$product_model = Mage::getModel('catalog/product');
	        
			$items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$glDebitAmount = 0;
				$glCreditAmount =0;
				
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=2) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=1) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				$glDebitAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("   --- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'gl_return.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
				
				
			} 
			unset($items);
			
			//=> 2. 3= ��ҵԴ��� / Installation
			
			//=> 3. 4= �������Թ��� / Packaging Fee
			
			//=> 4. 5= ��Һ�ԡ������ / Service
			
			//=> 5. 6= ��ҨѴ���Թ��� / Shipping Fee
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $ShippingAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=6) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			
			//=> 6. 7=Trade Discount  VAT
			
			//=> 6. 8=Trade Discount  Non VAT
			
			//=> 7. 9 = Selling VAT / VAT �ͧ��â��
			$VATAmount = $order->getTaxAmount();
			if ($VATAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $VATAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=9) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Vat Amount >0 = End
			}
			
			
			//=> Part 2 - Payment ******************/
			$glCategory = "Return";
			$RefundType = strtolower(trim($order->getPbsRefundType()));
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			//=> 3 = Credit and debit card
			if ($RefundType=="2c2p") {
				//=> 
				$OrdPaymentType = trim($order->getPaymentChannel());
				$OrdAgentChannel = trim($order->getChannelCode()) . "|" . trim($order->getAgentCode());
				
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 
				$query = "Select * From ttakeoff_payment Where (accpayment_type='". mysql_escape_string($OrdPaymentType) ."') and (accpayment_issuer_dealer='". mysql_escape_string($OrdAgentChannel) ."') " . 
							" and (accpayment_no=3) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				//echo $query;
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database
				//=> End
			}			
			
			//=> 4=Store Credit
			if (($RefundType=="storecredit") || ($OrderStoreCredit != 0)) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				if ($RefundType=="storecredit") {
					$glCreditAmount = $OrderAmount + $OrderStoreCredit;
				}
				else {
					$glCreditAmount =$OrderStoreCredit;
				}
				
				$query = "Select * From ttakeoff_payment Where (accpayment_no=4) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End
			}
			//-> Store Credit
			
			//=> 5=Transfer cash to Customer(EFT)
			if ($RefundType=="eft") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=5) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End				
			}
			//-> Transfer cash to Customer(EFT)
			
			//=> 6=cash
			if ($RefundType=="cash") {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=6) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				//=> End					
			}
			//-> cash
			
				//=> 1. Supplier discount

				
				//=> 2.Suspense Cash discount of Promotion
				if ($OrderDiscount>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = 0;
					$glCreditAmount = $OrderDiscount; 
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_no=2) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
						}
					}
					//=> 
					//=> Post GL Table 
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					$write->query($TakeOffGLSql);
					//=> End - Post to GL
				}
				

			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			//Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			//$order->setPbsPaidcompeteGl("Y");
			//$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			//Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		//else {
		//	Mage::log("--- GL - Exist", null, 'mgf.log');
		//}
		//=>
	}		
	
function createTTakeOffGL_Refund($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- 1. Create GL -> Customer Refund ---------", null, 'mgf.log');   
			//=> Path 1 - Income ******************/
			$glCategory = "Refund";//"Sale-PBS";
			$glOracleBranchCode ="035002";
			$glBranchShortName = "ECOM01";

			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');

			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',31,Now()";
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');
			
			
			//=> Part 2 - Payment ******************/
			$glCategory = "Refund";
			
			
			$RefundType = strtolower(trim($order->getPbsRefundType()));
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			//=> Transfer cash to Customer(EFT)
			if ($RefundType=="eft") {
				//=> 1. Cr. -
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=1) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				
				//=> 
				//=> 2. Dr. -
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = $OrderAmount; 
				$glCreditAmount = 0;

				$query = "Select * From ttakeoff_payment Where (accpayment_no=2) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);				
				
				//=> //End Save To Database			
				//=> End				
			}
			//-> Transfer cash to Customer(EFT)
			
			//=> 6=cash
			if ($RefundType=="cash") {
				//=> 3 - Cr.
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = 0;
				$glCreditAmount = $OrderAmount; 

				$query = "Select * From ttakeoff_payment Where (accpayment_no=3) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database			
				
				//=> 4. Dr
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
				$glDebitAmount = $OrderAmount; 
				$glCreditAmount = 0;

				$query = "Select * From ttakeoff_payment Where (accpayment_no=4) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$glCostCenterCode = $row["accpayment_costprofit_center"];
					}
				}
				
				//=> Save To Database
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
				$write->query($TakeOffGLSql);
				//=> //End Save To Database							
				
				//=> End					
			}
			//-> cash
			

			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			//Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			//$order->setPbsPaidcompeteGl("Y");
			//$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			//Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		//else {
		//	Mage::log("--- GL - Exist", null, 'mgf.log');
		//}
		//=>
	}		
	
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++
	function createTTakeOffCL_PBSCloseTheDoor($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("--- Export To GL | Close the door----------", null, 'mgf.log');
		//if ($order->getPbsShippedGl() !="Y") {
			//************ PART 1 : INCOME ***********************
			//=> Prepare Data
			//$glCategory = "Sale-PBS";
			//$glOracleBranchCode = "0301L8";
			//$glBranchShortName = "DCBN1";
			$glCategory = "Sale-PBS";
			$glOracleBranchCode = "035002";
			$glBranchShortName = "ECOM01";
			$glDebitAmount = 0;
			
			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',2,NOW()";
			
			//$glSaleDate = Mage::getModel('core/date')->date('d-M-y', strtotime($order->getCreatedAtFormated('full')));
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');
			$orderdatearr  =  explode(" ",$order->getCreatedAt());
			$orderdateOnlyArr = explode("-" , $orderdatearr[0]);
			$orderTimeOnlyArr = explode(":" , $orderdatearr[1]);
			$csrOrder_Date = Mage::getModel('core/date')->date('d-M-y',mktime(0, 0, 0, (int)$orderdateOnlyArr[1],(int)$orderdateOnlyArr[2], (int)$orderdateOnlyArr[0]));
			$csrOrder_Time = substr("00". $orderTimeOnlyArr[0] ,-2) . substr("00". $orderTimeOnlyArr[1] ,-2) . substr("00". $orderTimeOnlyArr[2] ,-2);
			$glSaleDate = $csrOrder_Date;
			echo "<p>Order Date : "  . $csrOrder_Date . "|" . $csrOrder_Time . " | " . $order->getCreatedAt() . "</p>";			
			
			
			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$product_model = Mage::getModel('catalog/product');
		
			//=> 101 = Installation - Cr
			
			//=> 102 = Packaging Fee - Cr
		
			//=> 103 = Service - Cr
		
			//=> 104 = Shipping Fee - Cr
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = 0; 
				$glCreditAmount = $ShippingAmount;
				$query = "Select * From ttakeoff_income Where (accincome_no=104) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase, gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}		
		
			//=> 105 = Trade Discount 
			/*
			$TradeDiscountAmount = abs($order->getBaseDiscountAmount());
			if ($TradeDiscountAmount>0) {
				//=> Get Trade Discount
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $TradeDiscountAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=105) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Trade Discount Amount>0 = End
			}		
			*/
		
			//=> 106 = Sale VAT, //=> 107 = Sale Non VAT
			
			//********************** xxxxxxxxxxxxxxxxxxxxxxxxxxxxx ****************//
	        $items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glDebitAmount = 0; $glCreditAmount = 0;
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=107) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=106) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				
				$glCreditAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL -> " . $ProductDivCode . " | "  . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase, gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
			} 
			unset($items);
			
			Mage::log("************ STRAT Suspense - xxxxxxxxxxxxxxxxxxx", null, 'mgf.log'); 
					
			//=> 108 / 109
			//= 108= Suspense  Sales  VAT, 109 = Suspense Sales  Non VAT
	        $items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glDebitAmount = 0; $glCreditAmount = 0;
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=109) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=108) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				
				$glDebitAmount = $item->getRowTotal();
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase, gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
			} 
			unset($items);
		
			//=> 110 = Suspense Installation
		
			//=> 111 = Suspense Packaging Fee
		
			//=> 112 = Suspense Service
		
			//=> 113 = Suspense Shipping Fee
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $ShippingAmount; 
				$glCreditAmount = 0;
				$query = "Select * From ttakeoff_income Where (accincome_no=113) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase, gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			else {
				Mage::log("******************** Shipping Amount = 0" , null, 'mgf.log');
			}
		
			//=> 114 = Suspense Trade Discount 
			/*
			$TradeDiscountAmount = abs($order->getBaseDiscountAmount());
			if ($TradeDiscountAmount>0) {
				//=> Get Trade Discount
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = 0; 
				$glCreditAmount = $TradeDiscountAmount;
				$query = "Select * From ttakeoff_income Where (accincome_no=114) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				//=> Trade Discount Amount>0 = End
			}			
			*/	
		
			//************ PATH 2 : Payment **********************//
			Mage::log("******************** Start GL Close the door - Payment" , null, 'mgf.log');
			$OrdPaymentBroker = $order->getPaymentbroker();
			$OrdPaymentChannel = $order->getPaymentChannel();
			$OrdChannelCode = $order->getChannelCode();
			//=> Payment 
			$OrderAmount = $order->getBaseGrandTotal();
			$PromotionDiscountAmount = abs($order->getBaseDiscountAmount());
			Mage::log('Payment : ' . $OrdPaymentBroker . ' | ' . $OrderAmount . ' | ' . $OrdPaymentChannel . ' | ' . $OrdChannelCode . ' | Promotion Discount =' . $PromotionDiscountAmount , null, 'mgf.log');
			
			if ($PromotionDiscountAmount > 0) {
				//=> 1. Dr.Cash discount of Promotion
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = $PromotionDiscountAmount; 
					$glCreditAmount = 0;
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_codetype=2) and (accpayment_no=1) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
							if (strtolower(trim($glSubAccountCode))=="dealer") {
							 	$glSubAccountCode = $OrdPaymentBroker;
							}
						}
					}
					//=> 
					//=> Post GL Table
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					Mage::log(" GL Pay : " . $TakeOffGLSql , null, 'mgf.log');
					$write->query($TakeOffGLSql);							
				
				
				//=> 2. Cr.Suspense Cash discount of Promotion
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = 0; 
					$glCreditAmount = $PromotionDiscountAmount;
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_codetype=2) and (accpayment_no=2) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
							if (strtolower(trim($glSubAccountCode))=="dealer") {
							 	$glSubAccountCode = $OrdPaymentBroker;
							}
						}
					}
					//=> 
					//=> Post GL Table
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase, gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					Mage::log(" GL Pay : " . $TakeOffGLSql , null, 'mgf.log');
					$write->query($TakeOffGLSql);			
				
			}
			//=> Order Amount>0 = End
			
			
			
			
			//************ PATH 3 : Update Status *****************//
			
			Mage::log(" Order Update Fix PSB Close The Door GL - Start : ", null, 'mgf.log');
			$order->setPbsShippedGl("Y");
			$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_shipped_gl','Y');
			Mage::log(" OOrder Update Fix PSB Close The Door GL - End : ", null, 'mgf.log');
			//***************** END Generate GL *****************
		
		//}
		//else {
		//	Mage::log("--- PSB Close the door - GL - Exist", null, 'mgf.log');
		//}
		//=>
	}	
	
	
	function createTTakeOffCL_PBSOrderApprove($order) {
		//$order = $observer->getEvent()->getOrder();
		echo "--- Create GL -> PSB Order Approve ----------";
		Mage::log("--- Create GL -> PSB Order Approve ----------", null, 'mgf.log');   
		//if ($order->getPbsPaidcompeteGl() !="Y") {
			//=> Build GL Data
			Mage::log("--- GL - Build", null, 'mgf.log');
			echo "--- GL - Build";
			//=> 1. Product VAT / Non VAT
			//= 1= Suspense Sales VAT, 2=Suspense SalesNon VAT
			
			$glCategory = "Sale-PBS";
			$glOracleBranchCode = "035002";
			$glBranchShortName = "ECOM01";
			$glDebitAmount = 0;
			
			$glRefCaseSql = ",'" . mysql_escape_string($order->getIncrementId()) . "',1,'". $order->getCreatedAt() ."'";
			
			//Mage::log($glCategory.":".$glOracleBranchCode.":".$glBranchShortName, null, 'mgf.log'); 
			
			//$glSaleDate = Mage::getModel('core/date')->date('d-M-y', strtotime($order->getCreatedAtFormated('full')));
			$glSaleDate = Mage::getModel('core/date')->date('d-M-y');
			
			$orderdatearr  =  explode(" ",$order->getCreatedAt());
			$orderdateOnlyArr = explode("-" , $orderdatearr[0]);
			$orderTimeOnlyArr = explode(":" , $orderdatearr[1]);
			$csrOrder_Date = Mage::getModel('core/date')->date('d-M-y',mktime(0, 0, 0, (int)$orderdateOnlyArr[1],(int)$orderdateOnlyArr[2], (int)$orderdateOnlyArr[0]));
			$csrOrder_Time = substr("00". $orderTimeOnlyArr[0] ,-2) . substr("00". $orderTimeOnlyArr[1] ,-2) . substr("00". $orderTimeOnlyArr[2] ,-2);
			$glSaleDate = $csrOrder_Date;
			echo "<p>Order Date : "  . $csrOrder_Date . "|" . $csrOrder_Time . " | " . $order->getCreatedAt() . "</p>";
			
			
			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
	
			$product_model = Mage::getModel('catalog/product');
	        
			$items = $order->getAllItems();
			foreach($items as $itemId => $item)
	        {
				$glDebitAmount = 0;
				$glCreditAmount =0;
				
				$_product = $product_model->load($item->getProductId()); // getting product details here to get description
				$ProductCostCenter = $_product->getData("cost_center");
				
				$glCostCenterCode = "";
				$glAccountCode = "";
				$glSubAccountCode = "";
				
				if ($item->getTaxAmount()==0) {
					//=> Non VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=2) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> End Non VAT
				}
				else {
					//=> VAT
					$query = "Select * From ttakeoff_income Where (accincome_no=1) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accincome_account_code"];
							$glSubAccountCode = $row["accincome_sub_account"];
							$CostProfitCode = $row["accincome_costprofit_center"];
						}
					}
					else {
						$glAccountCode = "-";
					}
					//=> VAT
				}
				$glCreditAmount = $item->getRowTotal();
				
				//=> 
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Summary Data
				$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
										$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
				Mage::log("--- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
				
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);
				
				
			} 
			unset($items);
			
			//=> 2. 3=Suspense Installation
			
			//=> 3. 4=Suspense Packaging Fee
			
			//=> 4. 5=Suspense Service Free
			
			//=> 5. 6=Suspense Shipping Fee
			$ShippingAmount = $order->getShippingAmount();
			if ($ShippingAmount>0) {
				//=> Get Account Code
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = 0; 
				$glCreditAmount = $ShippingAmount;
				$query = "Select * From ttakeoff_income Where (accincome_no=6) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Shipping Amount>0 = End
			}
			
			//=> 6. 7=Suspense Trade Discount
			/*
			$TradeDiscountAmount = abs($order->getBaseDiscountAmount());
			if ($TradeDiscountAmount>0) {
				//=> Get Trade Discount
				$CostProfitCode1 = "";
				$glAccountCode1 = "";
				$glSubAccountCode1 = "";
				
				$query = "Select * From ttakeoff_income Where (accincome_no=7) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode1 = $row["accincome_account_code"];
						$glSubAccountCode1 = $row["accincome_sub_account"];
						$CostProfitCode1 = $row["accincome_costprofit_center"];
					}
				}
				
				$items = $order->getAllItems();
				foreach($items as $itemId => $item)
		        {
					$glDebitAmount = 0;
					$glCreditAmount =0;
					
					$glCostCenterCode = "";
					$CostProfitCode = "";
					$glAccountCode = $glAccountCode1;
					$glSubAccountCode = $glSubAccountCode1;
					
					
					$_product = $product_model->load($item->getProductId()); // getting product details here to get description

					switch (strtolower(trim($CostProfitCode1))) {
					    case "product":
							$ProductDivCode = $_product->getData("cost_center");
					        $glCostCenterCode = $ProductDivCode;
					        break;
					default:
						$glCostCenterCode = $CostProfitCode1;
					}


					$glCreditAmount = $item->getRowTotal();
					
					//=> Summary Data
					$ExportData = $glCategory . "|" . $glOracleBranchCode . "|" . $glCostCenterCode . "|" . $glAccountCode . "|" . $glSubAccountCode . "|" . 
											$glSaleDate . "|" . $glDebitAmount . "|" . $glCreditAmount . "|" . $glBranchShortName  ;
					Mage::log("--- GL Product Items -> " . $ProductDivCode . " | " . $ExportData, null, 'mgf.log');  
					
					// now $write is an instance of Zend_Db_Adapter_Abstract
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
						"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
						" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase) " .
						" VALUES(" . 
						 	"'" . mysql_escape_string($glCategory) . "'," . 
						 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
						 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
						 	"'" . mysql_escape_string($glAccountCode) . "'," . 
						 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($glSaleDate) . "'," . 
						 	$glDebitAmount . "," . 
							$glCreditAmount . "," . 
						 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
							"" . //=> End Row 2
							$glRefCaseSql . 
						")";
					$write->query($TakeOffGLSql);
					
					
				} 
				unset($items);
				//=> Discount Amount>0 = End
			}
			*/
			
			//=> 7. 8=Selling VAT
			$VATAmount = $order->getTaxAmount();
			if ($VATAmount>0) {
				//=> Get Account Code
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = 0; 
				$glCreditAmount = $VATAmount;
				$query = "Select * From ttakeoff_income Where (accincome_no=8) and (accincome_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accincome_account_code"];
						$glSubAccountCode = $row["accincome_sub_account"];
						$CostProfitCode = $row["accincome_costprofit_center"];
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
				switch (strtolower(trim($CostProfitCode))) {
				    case "product":
				        $glCostCenterCode = $ProductCostCenter;
				        break;
				}
				
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				$write->query($TakeOffGLSql);			
				
				//=> Vat Amount >0 = End
			}
			
			
			//=> Part 2 - Payment ******************/
			$OrdPaymentBroker = $order->getPaymentbroker();
			
			$OrdPaymentChannel = $order->getPaymentChannel();
			$OrdChannelCode = $order->getChannelCode();
			$OrdAgentCode = $order->getAgentCode();
			//=> Payment 
			$OrderAmount = $order->getBaseGrandTotal();
			$OrderStoreCredit = $order->getCustomerBalanceAmount();
			$OrderDiscount = abs($order->getBaseDiscountAmount());
			
			Mage::log('Payment : ' . $OrdPaymentBroker . ' | ' . $OrderAmount . ' | ' . $OrdPaymentChannel . ' | ' . $OrdChannelCode . " | Store Credit = " . $OrderStoreCredit , null, 'mgf.log');
			
			
			
				//=> 9. Supplier discount
				
				//=> 10. Suspense Cash discount of Promotion
				if ($OrderDiscount>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = $OrderDiscount; 
					$glCreditAmount = 0;
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_codetype=1) and (accpayment_no=10) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
							if (strtolower(trim($glSubAccountCode))=="dealer") {
							 	$glSubAccountCode = $OrdPaymentBroker;
							}
						}
					}
					//=> 
					//=> Post GL Table
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					Mage::log(" GL Pay : " . $TakeOffGLSql , null, 'mgf.log');
					$write->query($TakeOffGLSql);						       					
					//=> End - Post to GL
				}
				
				
				//=> 11. Store Credit
				if ($OrderStoreCredit>0) {
					$glAccountCode = ""; $glSubAccountCode=""; $glCostCenterCode="";
					$glDebitAmount = $OrderStoreCredit; 
					$glCreditAmount = 0;
					//=> Get Account Code
					$query = "Select * From ttakeoff_payment Where (accpayment_codetype=1) and (accpayment_no=11) and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
					if ($AccArrays = $readConnection->fetchAll($query)) {
						foreach ($AccArrays as $row) {
							$glAccountCode = $row["accpayment_cccount_code"];
							$glSubAccountCode = $row["accpayment_sub_account"];
							$glCostCenterCode = $row["accpayment_costprofit_center"];
							if (strtolower(trim($glSubAccountCode))=="dealer") {
							 	$glSubAccountCode = $OrdPaymentBroker;
							}
						}
					}
					//=> 
					//=> Post GL Table
					// now $write is an instance of Zend_Db_Adapter_Abstract						
					$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
							"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
							" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
							" VALUES(" . 
							 	"'" . mysql_escape_string($glCategory) . "'," . 
							 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
							 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
							 	"'" . mysql_escape_string($glAccountCode) . "'," . 
							 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
								"" . 
							 	"'" . mysql_escape_string($glSaleDate) . "'," . 
							 	$glDebitAmount . "," . 
								$glCreditAmount . "," . 
							 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
								"" . //=> End Row 2
								$glRefCaseSql . 
							")";
					Mage::log(" GL Pay : " . $TakeOffGLSql , null, 'mgf.log');
					$write->query($TakeOffGLSql);						       					
					//=> End - Post to GL
				}

							//=> �����Թ��� 1-2-3, 2C2P
			if ($OrderAmount > 0) {
				//=> 	
				
				
									//=> ����
				$ProductCostCenter = "";
				$glAccountCode = ""; $glSubAccountCode=""; $CostProfitCode="";
				$glDebitAmount = $OrderAmount; 
				$glCreditAmount = 0;
				
				$query = "Select * From ttakeoff_payment Where  (accpayment_codetype=1) and (accpayment_no in (1,2,3)) " . 
						" and (accpayment_type='". mysql_escape_string($OrdPaymentChannel) ."') " .
						" and (accpayment_issuer_dealer='". mysql_escape_string($OrdChannelCode . "|" . $OrdAgentCode) ."') " . 
						" and (accpayment_category='" . mysql_escape_string($glCategory) . "') LIMIT 1";
				if ($AccArrays = $readConnection->fetchAll($query)) {
					foreach ($AccArrays as $row) {
						$glAccountCode = $row["accpayment_cccount_code"];
						$glSubAccountCode = $row["accpayment_sub_account"];
						$CostProfitCode = $row["accpayment_costprofit_center"];
						Mage::log(" GL Pay x  : " . $glSubAccountCode , null, 'mgf.log');
						if (strtolower(trim($glSubAccountCode))=="dealer") {
						 	$glSubAccountCode = $OrdPaymentBroker;
							Mage::log(" GL Pay y  : " . $glSubAccountCode , null, 'mgf.log');
						}
						
					}
				}
				//=> Summary Data
				$glCostCenterCode = $CostProfitCode;
						
	
				//=> Post GL Table
				// now $write is an instance of Zend_Db_Adapter_Abstract						
				$TakeOffGLSql = "Insert Into ttackoff_gl(" . 
					"gl_category, gl_oracle_branchcode, gl_costcenter_code, gl_account_code, gl_subaccount_code," . 
					" gl_sale_date, gl_debit_amount, gl_credit_amount, gl_branch_shortname,gl_orderno,	gl_glcase,gl_createddate) " .
					" VALUES(" . 
					 	"'" . mysql_escape_string($glCategory) . "'," . 
					 	"'" . mysql_escape_string($glOracleBranchCode) . "'," . 
					 	"'" . mysql_escape_string($glCostCenterCode) . "'," . 
					 	"'" . mysql_escape_string($glAccountCode) . "'," . 
					 	"'" . mysql_escape_string($glSubAccountCode) . "'," . 
						"" . 
					 	"'" . mysql_escape_string($glSaleDate) . "'," . 
					 	$glDebitAmount . "," . 
						$glCreditAmount . "," . 
					 	"'" . mysql_escape_string($glBranchShortName) . "' " . 
						"" . //=> End Row 2
						$glRefCaseSql . 
					")";
				Mage::log(" GL Pay : " . $TakeOffGLSql , null, 'mgf.log');
				$write->query($TakeOffGLSql);						        
						
				//=> Discount Amount>0 = End
			}				
			//=> Edn Crearte GL
			
			//=> Update Order Status - Start
			
			Mage::log(" Order Update Fix PSB Paid Complete GL - Start : ", null, 'mgf.log');
			$order->setPbsPaidcompeteGl("Y");
			$order->save();
			//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_gl','Y');
			Mage::log(" OOrder Update Fix PSB Paid Complete GL - End : ", null, 'mgf.log');
			//-> Update Order Status - End
			
		//}
		///else {
	//		Mage::log("--- GL - Exist", null, 'mgf.log');
	//		echo "--- GL - Exist";
	//	}
		//=>
	}
		
		
	function createTTakeOffCRS($order) {
		//$order = $observer->getEvent()->getOrder();
		Mage::log("createTTakeOffCRS", null, 'mgf.log');
		try 
		{
			$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			Mage::log(" Order Update Gen. CSR : " . $order->getPbsPaidcompeteCsr(), null, 'mgf.log');
			
			//if ($order->getPbsPaidcompeteCsr() !="Y") {
				//=> Start Create *******************************
				$storeId = Mage::app()->getStore()->getId();
				$OrderAmount = $order->getBaseGrandTotal();
				$OrderStoreCredit = $order->getCustomerBalanceAmount();
				$OrderDiscount = $order->getDiscountAmount();
				
				
				Mage::log(" cccccccccccc 100% Store Credit ( " . $OrderAmount . " | " . $OrderStoreCredit . "|" . $OrderDiscount . ")", null, 'mgf.log');   				
				
				$ItemsData = "";
				//$crsBatch_No = "e-commerce". date("ymd") ."01";
				Mage::log(" " . $order->getPbsPaidcompeteCsr(), null, 'mgf.log');
					$crsBatch_No = "";
					$csrStore_No = trim(Mage::getStoreConfig('ttfglcrs/default/srcstroreno', $storeId));
					$csrOrder_Date = Mage::getModel('core/date')->date('d/m/Y', strtotime($order->getCreatedAtFormated('short')));
					$csrOrder_Time = Mage::getModel('core/date')->date('His', strtotime($order->getCreatedAtFormated('short')));
					$csrOrder_Type = "S";
					$csrBroker_ID = $order->getPaymentbroker();
					
					$orderdatearr  =  explode(" ",$order->getCreatedAt());
					$orderdateOnlyArr = explode("-" , $orderdatearr[0]);
					$orderTimeOnlyArr = explode(":" , $orderdatearr[1]);
					$csrOrder_Date = substr("00". $orderdateOnlyArr[2] ,-2). "/" . substr("00". $orderdateOnlyArr[1] ,-2) . "/" . substr("0000". $orderdateOnlyArr[0] ,-4);
					$csrOrder_Time = substr("00". $orderTimeOnlyArr[0] ,-2) . substr("00". $orderTimeOnlyArr[1] ,-2) . substr("00". $orderTimeOnlyArr[2] ,-2);
					
					echo "<p>Order Date : "  . $csrOrder_Date . "|" . $csrOrder_Time . " | " . $order->getCreatedAt() . "</p>";
					
					$csrMerchant_Id = $order->getMerchantId();
					$csrOrder_Id = $order->getIncrementId();
					
					$csrInvoice_Date = "";
					$csrInvoice_No = "";
					
					$OrderInvoiceID = "";
					$OrderInvoiceDate = "";
					
					if ($order->hasInvoices()) {
						
					    foreach ($order->getInvoiceCollection() as $inv) {
					        $OrderInvoiceID = $inv->getIncrementId();
							$OrderInvoiceDate =  Mage::getModel('core/date')->date('d/m/Y', strtotime($inv->getCreatedAt()));
							//$OrderInvoiceDate =  $inv->getCreatedAt();
					    //other invoice details...
					    } 
						
					}
					
					$csrInvoice_No = $OrderInvoiceID;
					$csrInvoice_Date = $OrderInvoiceDate;
					
					
					$csrCredit_Note_No = "";
					$csrCredit_Note_Date = "";
		
					$csrPayment_Model = $order->getPaymentModel();
					
					$csrPayment_Channel = $order->getPaymentChannel();
					
					$csrChannel_Code = $order->getChannelCode();
					$csrAgent_Code = $order->getAgentCode();
					//=> Get VAT or None Vat			
					$csrSales_Vat = 0;
					$csrSales_NonVat = 0;
					$ItemVATAmount = 0; $ItemNonVATAmount = 0;
					//$product_model = Mage::getModel('catalog/product');
		        	$items = $order->getAllItems();
					foreach($items as $itemId => $item)
		        	{
						if ($item->getTaxAmount()==0) {
							$ItemNonVATAmount += $item->getRowTotal();
						}
						else {
							$ItemVATAmount += $item->getRowTotal();
						}
					
						//$_product = $product_model->load($item->getProductId()); // getting product details here to get description
						//$ItemsData .= $item->getItemId() . "|" . $item->getSku() . "|" . $item->getQtyOrdered() . "|". $item->getName() . "|". $_product->getData("cost_center") . "\n\r";
					
					} 
					unset($items);			
					$csrSales_Vat = $ItemVATAmount;
					$csrSales_NonVat = $ItemNonVATAmount;
					$csrVATsAmount = $order->getTaxAmount();
					
					
					$csrCouponNo = $order->getCouponCode();
					
					
					$csrCouponAmount = $order->getDiscountAmount();
					
					$csrShippingFee = $order->getShippingAmount();
					$csrShippingCharge_DiscountAmount = 0;
					$csrPackageingFee = 0;
					$csrOtherCharge = 0;
					
					$csrTotalOrderAmount = $order->getGrandTotal();
					$csrCurrencyCode =  $order->getOrderCurrencyCode();
					
					$csrApprovalCode = $order->getApprovalCode();
					$csrMasked_Pan = $order->getMaskedPan();
					$csrUser_defined_1 = $order->getUserDefined1();
					$csrUser_defined_2 = $order->getUserDefined2();
					$csrUser_defined_3 = $order->getUserDefined3();
					$csrUser_defined_4 = $order->getUserDefined4();
					$csrUser_defined_5 = $order->getUserDefined5();
					
					if ($OrderStoreCredit>0) {
						$OrderStoreCredit = -1 * $OrderStoreCredit;
					}
					
					Mage::log(" Order Items : \n\r" . $ItemsData, null, 'mgf.log');
					
					$TakeOffCrsSql = "";
					$TakeOffCrsSql .= "Insert Into ttakeoff_csr(" .
							"crs_createddate,batch_no,strore_no,order_date,order_time,order_type, broker_id,invoice_date,merchant_id,order_id,invoice_no," .
							"credit_note_no,credit_note_date,payment_model,payment_channel,channel_code,agent_code," .
							"sales_vat,sales_non_vat,vats_amount,coupon_no,coupon_amount,shipping_fee,shipping_charge_ciscount_amount,packageing_fee," .
							"other_charge, total_order_amount, currency_code, approval_code, masked_pan, " . 
							"user_defined_1, user_defined_2, user_defined_3, user_defined_4, user_defined_5, " .
							"store_credit_amount " . 
						 ") Values(" .
						 	"Now()," . 
						 	"'" . mysql_escape_string($crsBatch_No) . "'," . 
						 	"'" . mysql_escape_string($csrStore_No) . "'," . 
						 	"'" . mysql_escape_string($csrOrder_Date) . "'," . 
						 	"'" . mysql_escape_string($csrOrder_Time) . "'," . 
						 	"'" . mysql_escape_string($csrOrder_Type) . "'," . 
							"" . 
						 	"'" . mysql_escape_string($csrBroker_ID) . "'," . 
						 	"'" . mysql_escape_string($csrInvoice_Date) . "'," . 
						 	"'" . mysql_escape_string($csrMerchant_Id) . "'," . 
						 	"'" . mysql_escape_string($csrOrder_Id) . "'," . 
						 	"'" . mysql_escape_string($csrInvoice_No) . "'," . 
							"" .  //=> End Row 1
						 	"'" . mysql_escape_string($csrCredit_Note_No) . "'," . 
						 	"'" . mysql_escape_string($csrCredit_Note_Date) . "'," . 
						 	"'" . mysql_escape_string($csrPayment_Model) . "'," . 
						 	"'" . mysql_escape_string($csrPayment_Channel) . "'," . 
						 	"'" . mysql_escape_string($csrChannel_Code) . "'," . 
							"'" . mysql_escape_string($csrAgent_Code) . "'," . 
							"" . //=> End Row 2
						 	$csrSales_Vat . "," . 
							$csrSales_NonVat . "," . 
							$csrVATsAmount . "," . 
						 	"'" . mysql_escape_string($csrCouponNo) . "'," . 
							$csrCouponAmount . "," . 
							$csrShippingFee . "," . 
							$csrShippingCharge_DiscountAmount . "," . 
							$csrPackageingFee . "," . 
							"" . //=> End Row 3
							$csrOtherCharge . "," . 
							$csrTotalOrderAmount . "," . 
						 	"'" . mysql_escape_string($csrCurrencyCode) . "'," . 
						 	"'" . mysql_escape_string($csrApprovalCode) . "'," . 
						 	"'" . mysql_escape_string($csrMasked_Pan) . "'," . 
							"" . //=> End Row 4
						 	"'" . mysql_escape_string($csrUser_defined_1) . "'," . 
						 	"'" . mysql_escape_string($csrUser_defined_2) . "'," . 
						 	"'" . mysql_escape_string($csrUser_defined_3) . "'," . 
						 	"'" . mysql_escape_string($csrUser_defined_4) . "'," . 
						 	"'" . mysql_escape_string($csrUser_defined_5) . "', " . 
							$OrderStoreCredit . "" . 
						 ")"; 
					//echo $TakeOffCrsSql;
					
					Mage::log(" SQL : \n\r" . $TakeOffCrsSql, null, 'mgf.log');
					
					// fetch write database connection that is used in Mage_Core module
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					// now $write is an instance of Zend_Db_Adapter_Abstract
					$write->query($TakeOffCrsSql);			
					
					//=> Update Status
					Mage::log(" Order Update Fix xxxxxxxxxxxxxxxxx : ", null, 'mgf.log');
					$order->setPbsPaidcompeteCsr("Y");
					$order->save();
					//Mage::helper('ttf/sales')->saveOrderBySql($order->getIncrementId(), 'pbs_paidcompete_csr','Y');
					Mage::log(" Order Update Fix yyyyyyyyyyyyyyyyyyyyy : ", null, 'mgf.log');

				//=> End Create ************************
			//}
			//else {
			//	Mage::log(" ----- Exist CRS ********************", null, 'mgf.log');   
			//}
		} catch (Exception $e) { //Exception $e) {
			Mage::log((string)$e, null, 'mgf.log');   
		}
		
	}		
	
	function DeleteCRS($crsid) {
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					// now $write is an instance of Zend_Db_Adapter_Abstract
		$write->query("Delete from ttakeoff_csr where batch_id=" . $crsid);		
	
	}
?>