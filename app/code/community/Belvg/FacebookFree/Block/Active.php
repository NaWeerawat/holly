<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @email       roy@simpass.com 
*/

class Belvg_FacebookFree_Block_Active extends Mage_Core_Block_Template {

    public function getAppId()
    {
        return Mage::getStoreConfig('facebookfree/settings/appid');
    }

    public function getSecretKey()
    {
        return Mage::getStoreConfig('facebookfree/settings/secret');
    }

    public function isActiveLike()
    {
        return Mage::getStoreConfig('facebookfree/like/enabled');
    }

    public function isFacesLikeActive()
    {
        return Mage::getStoreConfig('facebookfree/like/faces')?'true':'false';
    }

    public function getLikeWidth()
    {
        return Mage::getStoreConfig('facebookfree/like/width');
    }

    public function getLikeColor()
    {
        return Mage::getStoreConfig('facebookfree/like/color');
    }

    public function getLikeLayout()
    {
        return Mage::getStoreConfig('facebookfree/like/layout');
    }

    public function checkFbUser()
    {
	$user_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
	$uid = 0;
	$db_read = Mage::getSingleton('core/resource')->getConnection('facebookfree_read');
	$tablePrefix = (string)Mage::getConfig()->getTablePrefix();
        
	$sql = 'SELECT `fb_id`
		FROM `'.$tablePrefix.'belvg_facebook_customer`
		WHERE `customer_id` = '.$user_id.'
		LIMIT 1';
	$data = $db_read->fetchRow($sql);
	if (count($data)) {
	  $uid = $data['fb_id'];
	}
	return $uid;
    }	
}