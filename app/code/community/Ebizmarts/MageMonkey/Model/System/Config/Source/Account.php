<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Model_System_Config_Source_Account
{

	/**
	 * Account details storage
	 *
	 * @access protected
	 * @var bool|array
	 */
    protected $_account_details = FALSE;

	/**
	 * Set AccountDetails on class property if not already set
	 *
	 * @return void
	 */
    public function __construct()
    {
        if (!$this->_account_details) {
            $this->_account_details = Mage::getSingleton('monkey/api')
                    ->getAccountDetails();
        }
    }

	/**
	 * Return data if API key is entered
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        if(is_array($this->_account_details)){
            return array(
                array('value' => 0, 'label' => Mage::helper('monkey')->__('Username:') . ' ' . $this->_account_details['username']),
                array('value' => 1, 'label' => Mage::helper('monkey')->__('Plan type:') . ' ' . $this->_account_details['plan_type']),
                array('value' => 2, 'label' => Mage::helper('monkey')->__('Is in trial mode?:') . ' ' . ($this->_account_details['is_trial'] ? Mage::helper('monkey')->__('Yes') : Mage::helper('monkey')->__('No')))
            );
        }else{
            return array(array('value' => '', 'label' => Mage::helper('monkey')->__('--- Enter your API KEY first ---')));
        }
    }

}
