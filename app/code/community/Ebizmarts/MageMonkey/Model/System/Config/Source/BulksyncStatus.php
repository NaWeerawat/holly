<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/


class Ebizmarts_MageMonkey_Model_System_Config_Source_BulksyncStatus
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
        	array('value' => 'idle', 'label' => Mage::helper('monkey')->__('IDLE')),
            array('value' => 'running', 'label' => Mage::helper('monkey')->__('Running Now')),
            array('value' => 'chunk_running', 'label' => Mage::helper('monkey')->__('Running')),
            array('value' => 'finished', 'label' => Mage::helper('monkey')->__('Completed')),
        );
    }

	/**
	 * Another way of returning data
	 *
	 * @return array
	 */
    public function toOption()
    {
    	$options = $this->toOptionArray();

    	$ary = array();

    	foreach($options as $option){
			$ary[$option['value']] = $option['label'];
    	}

    	return $ary;
    }

}