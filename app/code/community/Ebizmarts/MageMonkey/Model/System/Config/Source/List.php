<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/


class Ebizmarts_MageMonkey_Model_System_Config_Source_List
{

	/**
	 * Lists for API key will be stored here
	 *
	 * @access protected
	 * @var array Email lists for given API key
	 */
	protected $_lists   = null;

	/**
	 * Load lists and store on class property
	 *
	 * @return void
	 */
	public function __construct()
	{
		if( is_null($this->_lists) ){
			$this->_lists = Mage::getSingleton('monkey/api')
							->lists();
		}
	}

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
    	$lists = array();

    	if(is_array($this->_lists)){

    		foreach($this->_lists['data'] as $list){
    			$lists []= array('value' => $list['id'], 'label' => $list['name'] . ' (' . $list['stats']['member_count'] . ' ' . Mage::helper('monkey')->__('members') . ')');
    		}

    	}else{
    		$lists []= array('value' => '', 'label' => Mage::helper('monkey')->__('--- No data ---'));
    	}

        return $lists;
    }

}
