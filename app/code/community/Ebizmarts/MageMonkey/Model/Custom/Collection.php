<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Model_Custom_Collection
	extends Varien_Data_Collection
{

	/**
	 * Contains generic data to load on load() method
	 *
	 * @var mixed
	 */
	protected $_toload = NULL;

	/**
	 * Initialize data to be loaded afterwards
	 *
	 * @param array $data
	 * @return Varien_Data_Collection
	 */
	public function __construct(array $data)
	{
		$data = current($data);

		if( empty($data) ){
			return parent::__construct();
		}

		$this->_toload = $data;

		return parent::__construct();
	}

	/**
	 * Load data into object
	 *
	 * @param bool $printQuery
	 * @param bool $logQuery
	 * @return Ebizmarts_MageMonkey_Model_Custom_Collection
	 */
	public function load($printQuery = false, $logQuery = false)
	{
		if($this->isLoaded() || is_null($this->_toload)){
			return $this;
		}

        foreach ($this->_toload as $row) {
            $item = new Varien_Object;
            $item->addData($row);
            $this->addItem($item);
        }

        $this->_setIsLoaded();

		return $this;
	}
}