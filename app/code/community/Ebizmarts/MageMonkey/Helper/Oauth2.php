<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Helper_Oauth2 extends Mage_Core_Helper_Abstract {

	protected $_authorizeUri     = "https://login.mailchimp.com/oauth2/authorize";
	protected $_accessTokenUri   = "https://login.mailchimp.com/oauth2/token";
	protected $_redirectUri      = "http://ebizmarts.com/magento/mailchimp/oauth2/complete.php";
	protected $_clientId         = 213915096176;

	public function authorizeRequestUrl() {

		$url = $this->_authorizeUri;
		$redirectUri = urlencode($this->_redirectUri);
		
		return "{$url}?redirect_uri={$redirectUri}&response_type=code&client_id={$this->_clientId}";
	}
}
