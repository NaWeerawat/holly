<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/
class Ebizmarts_MageMonkey_Helper_Export extends Mage_Core_Helper_Abstract
{
	/**
	 * Parse members data
	 *
	 * @param string $response JSON encoded
	 * @param array $listMergeVars MergeFields for this list from MC
	 * @param string $store
	 * @return array
	 */
	public function parseMembers($response, $listMergeVars, $store)
	{

		$storeId = Mage::app()->getStore($store)->getId();

		//Explode response, one record per line
		$response = explode("\n", $response);

		//My Merge Vars
		$mergeMaps = Mage::helper('monkey')->getMergeMaps($storeId);

		//Get Header (MergeVars)
		$header = json_decode(array_shift($response));

		//Add var to maps, not included on config
		array_unshift($mergeMaps, array('magento' => 'email', 'mailchimp' => 'EMAIL'));

		$canMerge = array();
		foreach($header as $mergePos => $mergeLabel){
			foreach($listMergeVars as $var){
				if( strcmp($mergeLabel, $var['name']) === 0 ){

					foreach($mergeMaps as $map){
						if(strcmp($var['tag'], $map['mailchimp']) === 0){
							$canMerge [$mergePos]= $map['magento'];
						}
					}

				}
			}
		}

		$membersData = array();

        foreach($response as $member){
		    if (trim($member) != ''){
		      $membersData []= array_combine($canMerge, array_intersect_key(json_decode($member), $canMerge));
		    }
        }

		return $membersData;
	}

}