<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_WebhookController extends Mage_Core_Controller_Front_Action
{

	/**
	 * Entry point for all webhook operations
	 */
	public function indexAction()
	{

		$requestKey = $this->getRequest()->getParam('wkey');

		//Checking if "wkey" para is present on request, we cannot check for !isPost()
		//because Mailchimp pings the URL (GET request) to validate webhook
		if( !$requestKey ){
			$this->getResponse()
            	->setHeader('HTTP/1.1', '403 Forbidden')
            	->sendResponse();
        	return $this;
		}

		Mage::helper('monkey')->log( print_r($this->getRequest()->getPost(), true) );

		Mage::app()->setCurrentStore(Mage::app()->getDefaultStoreView());

		$data  = $this->getRequest()->getPost('data');
		$myKey = Mage::helper('monkey')->getWebhooksKey(null, $data['list_id']);

		//Validate "wkey" GET parameter
		if ($this->getRequest()->getPost('type')) {
			Mage::getModel('monkey/monkey')->processWebhookData($this->getRequest()->getPost());
		} else {
		    Mage::helper('monkey')->log($this->__('WebHook Key invalid! Key Request: %s. My Key: %s', $requestKey, $myKey));
		}


	}

}
