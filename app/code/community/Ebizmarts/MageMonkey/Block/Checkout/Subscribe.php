<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Block_Checkout_Subscribe extends Ebizmarts_MageMonkey_Block_Lists
{

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$alreadySubscribed = Mage::getModel('newsletter/subscriber')
							->loadByEmail($this->getQuote()->getCustomerEmail())
							->isSubscribed();
		/**
		 * If you don't want to show the lists in the checkout when the user it's already subscribed.
		 * Replace the code below for the condition below
		 *
		 * 	if ( !$this->helper('monkey')->canCheckoutSubscribe() OR $alreadySubscribed ) {
		 *
		 *
		 * **/

        if ( !$this->helper('monkey')->canCheckoutSubscribe() ) {
            return '';
        }

        return parent::_toHtml();
    }

	/**
	 * Retrieve current quote object from session
	 *
	 * @return Mage_Sales_Model_Quote
	 */
    public function getQuote()
    {
    	return Mage::getSingleton('checkout/session')
    			->getQuote();
    }

	/**
	 * Retrieve from config the status of the checkbox
	 *
	 * @see Ebizmarts_MageMonkey_Model_System_Config_Source_Checkoutsubscribe
	 * @return integer Config value possible values are 0,1,2,3
	 */
	public function checkStatus()
	{
		return (int)$this->helper('monkey')->config('checkout_subscribe');
	}

}
