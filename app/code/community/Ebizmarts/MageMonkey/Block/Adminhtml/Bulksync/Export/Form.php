<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Block_Adminhtml_Bulksync_Export_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post'
        ));

        $fieldset   = $form->addFieldset('export_settings', array(
            'legend'    => Mage::helper('monkey')->__('Export Configuration')
        ));

		$dataSource = Mage::getSingleton('monkey/system_config_source_bulksyncDatasource')->toOptionArray();
        $fieldset->addField('data_source_entity', 'select', array(
            'label'     => Mage::helper('monkey')->__('Data Source'),
            'title'     => Mage::helper('monkey')->__('Data Source'),
            'name'      => 'data_source_entity',
            'values'   => $dataSource,
            'class' => 'required-entry',
            'required' => true,
        ));

		$lists = Mage::getSingleton('monkey/system_config_source_list')->toOptionArray();
        $fieldset->addField('list', 'multiselect', array(
            'label'     => Mage::helper('monkey')->__('Choose Lists'),
            'title'     => Mage::helper('monkey')->__('Choose Lists'),
            'name'      => 'list',
            'values'   => $lists,
            'class' => 'required-entry',
            'required' => true,
        ));

        $storeSwitcher = $fieldset->addField('store_id', 'select', array(
            'name'      => 'store_id',
            'label'     => Mage::helper('monkey')->__('Store'),
            'title'     => Mage::helper('monkey')->__('Store'),
            'required'  => true,
            'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
        ));

        $fieldset->addField('direction', 'hidden', array(
            'name'     => 'direction',
            'value'    => 'export',
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        $this->setChild('form_after', Mage::getModel('core/layout')->createBlock('monkey/adminhtml_bulksync_queueExport', 'exportqueue'));

        return parent::_prepareForm();
    }

}
