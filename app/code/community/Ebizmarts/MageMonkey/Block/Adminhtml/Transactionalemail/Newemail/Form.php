<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_MageMonkey_Block_Adminhtml_Transactionalemail_Newemail_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/validateEmail'),
            'method'    => 'post'
        ));

        $fieldset   = $form->addFieldset('newemail_data', array(
            'legend'    => Mage::helper('monkey')->__('New email')
        ));

        $fieldset->addField('email_address', 'text', array(
            'label'     => Mage::helper('monkey')->__('Email address'),
            'title'     => Mage::helper('monkey')->__('Email address'),
            'name'      => 'email_address',
            'class' => 'validate-email',
            'required' => true,
        ));

        $fieldset->addField('service', 'hidden', array(
            'name'     => 'service',
            'value'    => $this->getRequest()->getParam('service'),
        ));
        $fieldset->addField('store', 'hidden', array(
            'name'     => 'store',
            'value'    => $this->getRequest()->getParam('store', 0),
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

		return parent::_prepareForm();

    }

}
