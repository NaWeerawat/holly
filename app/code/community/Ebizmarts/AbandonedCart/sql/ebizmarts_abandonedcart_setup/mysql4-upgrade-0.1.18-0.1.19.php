<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

$installer = $this;

$installer->startSetup();

$installer->run("
	CREATE TABLE IF NOT EXISTS `{$this->getTable('magemonkey_mails_sent')}` (
	  `id` INT(10) unsigned NOT NULL auto_increment,
	  `store_id` smallint(5),
	  `mail_type` ENUM('abandoned cart','happy birthday','new order', 'related products', 'product review', 'no activity', 'wishlist') NOT NULL,
	  `customer_email` varchar(255),
	  `customer_name` varchar(255),
	  `coupon_number` varchar(255),
	  `coupon_type` smallint(2),
	  `coupon_amount` decimal(10,2),
      `sent_at` DATETIME NOT NULL ,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();