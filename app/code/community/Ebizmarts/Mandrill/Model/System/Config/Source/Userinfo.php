<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_Mandrill_Model_System_Config_Source_Userinfo
{

	/**
	 * Account details storage
	 *
	 * @access protected
	 * @var bool|array
	 */
    protected $_account_details;

	/**
	 * Set AccountDetails on class attribute if not already set
	 *
	 * @return void
	 */
    public function __construct()
    {
        if (!$this->_account_details) {
            $helper = Mage::helper('mandrill');
            $this->_account_details = $helper->api()
                                             ->setApiKey($helper->getApiKey())
                                             ->usersInfo();
        }
    }

	/**
	 * Return data if API key is entered
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        $helper = Mage::helper('mandrill');
        if(is_object($this->_account_details)){

            $this->_account_details = (array)$this->_account_details;            

            return array(
                array('value' => 0, 'label' => $helper->__("<strong>Username</strong>: %s %s", $this->_account_details["username"], "<small>used for SMTP authentication</small>")),

                array('value' => 1, 'label' => $helper->__('<strong>Reputation</strong>: %s %s', $this->_account_details['reputation'], "<small>scale from 0 to 100, with 75 generally being a \"good\" reputation</small>")),

                array('value' => 2, 'label' => $helper->__('<strong>Hourly Quota</strong>: %s %s', $this->_account_details['hourly_quota'], "<small>the maximum number of emails Mandrill will deliver for this user each hour. Any emails beyond that will be accepted and queued for later delivery. Users with higher reputations will have higher hourly quotas</small>")),

                array('value' => 3, 'label' => $helper->__('<strong>Backlog</strong>: %s %s', $this->_account_details['backlog'], "<small>the number of emails that are queued for delivery due to exceeding your monthly or hourly quotas</small>"))
            );
        }else{
            return array(array('value' => '', 'label' => $helper->__('--- Enter your API KEY first ---')));
        }
    }

}
