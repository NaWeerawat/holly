<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_Mandrill_Helper_Data extends Mage_Core_Helper_Abstract	 {

	private $_configPath = 'mandrill/general/';

	/**
	 * Check if Mandrill is enabled
	 *
	 * @return bool
	 */
	public function useTransactionalService() {
		$active = Mage::getStoreConfigFlag($this->_configPath . "active");
		$key    = $this->getApiKey();

		return ($active && (strlen($key)));
	}

	public function api() {
		return new Mandrill_API();
	}

	/**
	* Retrieves Mandrill API KEY from Magento's configuration
	*
	* @return string
	*/
	public function getApiKey($store = null) {
		return Mage::getStoreConfig($this->_configPath . "apikey",$store);
	}

	/**
	 * Get module User-Agent to use on API requests
	 *
	 * @return string
	 */
	public function getUserAgent() {
		$modules = Mage::getConfig()->getNode('modules')->children();
		$modulesArray = (array)$modules;

		$aux = (array_key_exists('Enterprise_Enterprise',$modulesArray))? 'EE' : 'CE' ;
		$v = (string)Mage::getConfig()->getNode('modules/Ebizmarts_Mandrill/version');
		$version = strpos(Mage::getVersion(),'-')? substr(Mage::getVersion(),0,strpos(Mage::getVersion(),'-')) : Mage::getVersion();
		return (string)'Ebizmarts_Mandrill'.$v.'/Mage'.$aux.$version;
	}

	/**
	 * Logging facility
	 *
	 * @param mixed $data Message to save to file
	 * @param string $filename log filename, default is <Monkey.log>
	 * @return Mage_Core_Model_Log_Adapter
	 */
	public function log($data, $filename = 'Ebizmarts_Mandrill.log') {
		if(Mage::getStoreConfig($this->_configPath . "enable_log")) {
			return Mage::getModel('core/log_adapter', $filename)->log($data);
		}
	}

}