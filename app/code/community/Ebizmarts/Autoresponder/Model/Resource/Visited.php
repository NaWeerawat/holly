<?php

/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

class Ebizmarts_Autoresponder_Model_Resource_Visited extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('ebizmarts_autoresponder/visited','id');
    }
    public function loadByCustomerProduct(Ebizmarts_Autoresponder_Model_Visited $obj,$customerId,$productId,$storeId) {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getMainTable())
            ->where($this->getMainTable().'.'.'customer_id =?',$customerId)
            ->where($this->getMainTable().'.'.'product_id =?',$productId)
            ->where($this->getMainTable().'.'.'store_id =?',$storeId);
        $visited_id = $this->_getReadAdapter()->fetchOne($select);
        if($visited_id) {
            $this->load($obj,$visited_id);
        }
        return $this;
    }
}