<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magentocommerce.com for more information.
*
* @copyright Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* @email roy@simpass.com 
*/

$installer = $this;

$installer->startSetup();

$installer->run("

	CREATE TABLE IF NOT EXISTS `{$this->getTable('ebizmarts_autoresponder_review')}` (
	  `id` int(10) unsigned NOT NULL auto_increment,
	  `customer_id` int(10),
	  `store_id` smallint(5),
	  `items` smallint(5) default 0,
	  `counter` smallint(5) default 0,
	  `token` varchar(255) default null,
	  `order_id` int(10) unsigned not null,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
    ALTER TABLE `{$this->getTable('magemonkey_mails_sent')}`
     CHANGE `mail_type` `mail_type` ENUM( 'abandoned cart', 'happy birthday', 'new order', 'related products', 'product review', 'no activity', 'wishlist', 'review coupon' )
     CHARACTER SET utf8 NOT NULL;
");

$installer->addAttribute(
    'customer',
    'ebizmarts_reviews_cntr_total',
    array(
        'type'                 => 'int',
        'input'                => 'hidden',
        'required'             => 0,
        'default'              => 0,
        'visible_on_front'     => 0,
        'user_defined'         => true,
    )
);
$installer->addAttribute(
    'customer',
    'ebizmarts_reviews_coupon_total',
    array(
        'type'                 => 'int',
        'input'                => 'hidden',
        'required'             => 0,
        'default'              => 0,
        'visible_on_front'     => 0,
        'user_defined'         => true,
    )
);

$installer->endSetup();